package com.example.shariqkhan.gawadar247;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.Utils.NavViewHelper;
import com.example.shariqkhan.gawadar247.adapters.FavouritePagerAdapter;
import com.example.shariqkhan.gawadar247.adapters.onTaskCompleted;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.irozon.sneaker.Sneaker;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Syed Talha Hai on 30-Nov-17.
 */

public class DistrictMapSingleImage extends AppCompatActivity {
    Toolbar toolbar;
    private BottomNavigationViewEx bottomNavigationViewEx;
    TextView toolbarText;
    ImageView imageView;

    public static String URL = "http://www.gwadar247.pk/api/districtmap.php?";
    public String getid;
    public String gettoken;
    public SharedPreferences prefs;

    public ProgressDialog progressDialog;
    WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.districtmapsingleimage);
        overridePendingTransition(0, 0);
        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);
        bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottom_nav_bar);
        NavViewHelper.modifyBottomNavBarEx(bottomNavigationViewEx);
        NavViewHelper.enableNavigation(DistrictMapSingleImage.this, bottomNavigationViewEx);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(0);


        toolbarText = (TextView) toolbar.findViewById(R.id.tollbarText);
        toolbarText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        webView = (WebView) findViewById(R.id.image_webview);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        getid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);

        new getImage(new onTaskCompleted() {
            @Override
            public void onTaskCompleted(String code) {
                webView.getSettings().setBuiltInZoomControls(true);
                webView.getSettings().setSupportZoom(true);
                webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
                webView.getSettings().setUseWideViewPort(true);
                webView.getSettings().setLoadWithOverviewMode(true);

                webView.loadUrl(code);
                webView.setWebViewClient(new MyWebViewClient());
            }
        }).execute();

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressDialog.show();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressDialog.dismiss();
            super.onPageFinished(view, url);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private class getImage extends AsyncTask<Object, Object, String> {

        onTaskCompleted onTaskCompleted;


        public getImage(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }

        @Override
        protected String doInBackground(Object... voids) {


            String url =  URL + "?userid=" + getid + "&" + "token=" + gettoken;
            //  Log.e("logout", url);


            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);
                if(jsonObject.has("Image")) {

                    String image = jsonObject.getString("Image");
                    onTaskCompleted.onTaskCompleted(image);

                }
//                else if (getCode.equals("802")) {
//
//                    SharedPreferences.Editor edit = prefs.edit();
//                    edit.clear();
//                    edit.commit();
//
//                    Sneaker.with((Activity) getApplicationContext())
//                            .setTitle("Error!")
//                            .setDuration(5000)
//                            .setMessage("Login Again")
//                            .sneakError();
//
//                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    getApplicationContext().startActivity(intent);
//                }
                else {
                    progressDialog.dismiss();

                    Sneaker.with(DistrictMapSingleImage.this)
                            .setTitle("Error!")
                            .setDuration(5000)
                            .setMessage("Something Went Wrong! Please Relogin.")
                            .sneakWarning();
                }

            } catch (JSONException e) {
                Sneaker.with(DistrictMapSingleImage.this)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DistrictMapSingleImage.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }
}
