package com.example.shariqkhan.gawadar247;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.shariqkhan.gawadar247.FavouriteSales;
import com.example.shariqkhan.gawadar247.FavouriteWanted;


public class FilteredPagerAdapter extends FragmentPagerAdapter {
    public FilteredPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FilteredSales wanted = new FilteredSales();
                return wanted;
            case 1:
                FilteredWanted sales = new FilteredWanted();
                return sales;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Sales";
            case 1:
                return "Wanted";
            default:
                return null;
        }

    }
}


