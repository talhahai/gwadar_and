package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.models.LoginModel;
import com.example.shariqkhan.gawadar247.models.newsFlashModel;
import com.google.gson.Gson;
import com.irozon.sneaker.Sneaker;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DistrictMap extends AppCompatActivity {

    public static int Act_Num = 1;
    BottomNavigationViewEx bottomNavigationViewEx;
    TextView textView;
    public static int activityCheck = 0;
    Toolbar toolbar;
    public static String BASE_URL = "http://www.gwadar247.pk/api/districtmaps.php?";
    SharedPreferences prefs;
    private String getuserid;
    public ProgressDialog progressDialog;
    private String gettoken;
    ArrayList<newsFlashModel> listViewItems = new ArrayList<>();

    RecyclerView recyclerView;

    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_district_map);
        toolbar = (Toolbar) findViewById(R.id.main_app_bar);

        textView = (TextView) toolbar.findViewById(R.id.tollbarText);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        setSupportActionBar(toolbar);


        prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);

        getuserid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);

toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.property_recycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayShowTitleEnabled(false);


        Task task = new Task();
        task.execute();


    }

    private class Task extends AsyncTask<String, Void, String> {
        String stream = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DistrictMap.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading");

            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String[] params) {

            String getResponse = getJson();
            stream = getResponse;

            return stream;

        }

        private String getJson() {
            HttpClient httpClient = new DefaultHttpClient();
            String urltoSend = BASE_URL + "userid=" + getuserid + "&" + "token=" + gettoken;

            HttpPost post = new HttpPost(urltoSend);

            StringBuilder buffer = new StringBuilder();

            try {
                // Log.e("Insidegetjson", "insidetry");
                HttpResponse response = httpClient.execute(post);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String Line = "";

                while ((Line = reader.readLine()) != null) {
                    Log.e("buffer", buffer.toString());
                    buffer.append(Line);

                }
                reader.close();

            } catch (Exception o) {
                Log.e("Error", o.getMessage());

            }
            return buffer.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonobj;
            Log.e("Res", s);
            if (s != null) {
                try {
                    jsonobj = new JSONObject(s);


                    String errorCode = jsonobj.getString("ErrorCode");

                    if (errorCode.equals("000")) {

                        JSONArray jsonArray = jsonobj.getJSONArray("Maps");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            newsFlashModel model = new newsFlashModel();
                            JSONObject js = jsonArray.getJSONObject(i);

                            model.id = js.getString("ID");
                            model.date = js.getString("Name");
                            model.image = js.getString("Image");
                            model.thumb = js.getString("Thumb");

                            listViewItems.add(model);


                        }
                        adapter = new favouriteAdapter6(listViewItems, DistrictMap.this);
                        recyclerView.setAdapter(adapter);

                        progressDialog.dismiss();


                    }


                } catch (JSONException e) {
                    Sneaker.with(DistrictMap.this)
                            .setTitle("No internet connection!")
                            .setDuration(5000)
                            .setMessage("No internet connection. Retry with stable connection")
                            .sneakError();
                    progressDialog.dismiss();
                }


            }

            progressDialog.dismiss();
        }
    }

}
