package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.models.newsFlashModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.irozon.sneaker.Sneaker;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    Toolbar toolbar;
    TextView toolbarText;
    ImageView imageView;

    private GoogleMap mMap;

    //  public static String BASE_URL = "http://www.gwadar247.pk/api/gwadarmappins.php?";
    private String gettoken;
    SharedPreferences prefs;
    public static String url;
    private String getuserid;
    public ProgressDialog progressDialog;
    ArrayList<newsFlashModel> listViewItems = new ArrayList<>();
    ImageView back, center;

    SupportMapFragment mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);

        getuserid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);


        overridePendingTransition(0, 0);
        back = (ImageView)findViewById(R.id.back);
        center = (ImageView)findViewById(R.id.center);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapFragment.getMapAsync(MapsActivity.this);
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

//        LatLng gawadar = new LatLng(25.1987, 62.3213);
//        LatLng cmp = mMap.getCameraPosition().target;
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.marker2);

        for (int i = 0; i < MainActivity.arrayOfPins.length; i++) {


            mMap.addMarker(new MarkerOptions().position(MainActivity.arrayOfPins[i]).title(MainActivity.titles[i]).snippet(MainActivity.Ids[i]).icon(icon));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MainActivity.arrayOfPins[i], 12.8f));
        }
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                String getId = marker.getTitle();
                Log.e("PinId", getId);
                url = "http://www.gwadar247.pk/api/searchbypin2.php?" + "userid=" + getuserid + "&token=" + gettoken + "&pinid=" + getId;
                Intent intent = new Intent(MapsActivity.this, SearchResultOfPins.class);
                startActivity(intent);
                return true;
            }
        });
        //   mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }


    private class Task extends AsyncTask<String, Void, String> {
        String stream = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MapsActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading");

            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String[] params) {

            String getResponse = getJson();
            stream = getResponse;

            return stream;

        }

        private String getJson() {
            HttpClient httpClient = new DefaultHttpClient();


            HttpPost post = new HttpPost(url);

            StringBuilder buffer = new StringBuilder();

            try {
                // Log.e("Insidegetjson", "insidetry");
                HttpResponse response = httpClient.execute(post);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String Line = "";

                while ((Line = reader.readLine()) != null) {
                    Log.e("buffer", buffer.toString());
                    buffer.append(Line);

                }
                reader.close();

            } catch (Exception o) {
                Log.e("Error", o.getMessage());

            }
            return buffer.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonobj;
            Log.e("Res", s);
            if (s != null) {
                try {
                    jsonobj = new JSONObject(s);


                    String errorCode = jsonobj.getString("ErrorCode");

                    if (errorCode.equals("000")) {

                        JSONArray jsonArray = jsonobj.getJSONArray("Pins");


                        progressDialog.dismiss();


                    }


                } catch (JSONException e) {
                    Sneaker.with(MapsActivity.this)
                            .setTitle("No internet connection!")
                            .setDuration(5000)
                            .setMessage("No internet connection. Retry with stable connection")
                            .sneakError();
                    progressDialog.dismiss();
                }


            }

            progressDialog.dismiss();
        }
    }

}
