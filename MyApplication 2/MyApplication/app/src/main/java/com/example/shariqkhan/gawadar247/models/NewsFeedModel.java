package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 9/22/2017.
 */

public class NewsFeedModel extends BaseClass {
    private String ID;

    public String getID() { return this.ID; }

    public void setID(String ID) { this.ID = ID; }

    private String Rating;

    public String getRating() { return this.Rating; }

    public void setRating(String Rating) { this.Rating = Rating; }

    private String IsSold;

    public String getIsSold() { return this.IsSold; }

    public void setIsSold(String IsSold) { this.IsSold = IsSold; }

    private String IsOpenLand;

    public String getIsOpenLand() { return this.IsOpenLand; }

    public void setIsOpenLand(String IsOpenLand) { this.IsOpenLand = IsOpenLand; }

    private String Price;

    public String getPrice() { return this.Price; }

    public void setPrice(String Price) { this.Price = Price; }

    private String Image;

    public String getImage() { return this.Image; }

    public void setImage(String Image) { this.Image = Image; }

    private String IsRequested;

    public String getIsRequested() { return this.IsRequested; }

    public void setIsRequested(String IsRequested) { this.IsRequested = IsRequested; }

    private String IsFavorite;

    public String getIsFavorite() { return this.IsFavorite; }

    public void setIsFavorite(String IsFavorite) { this.IsFavorite = IsFavorite; }

    private String Line1Icon;

    public String getLine1Icon() { return this.Line1Icon; }

    public void setLine1Icon(String Line1Icon) { this.Line1Icon = Line1Icon; }

    private String Line1Text;

    public String getLine1Text() { return this.Line1Text; }

    public void setLine1Text(String Line1Text) { this.Line1Text = Line1Text; }

    private String Line2Icon;

    public String getLine2Icon() { return this.Line2Icon; }

    public void setLine2Icon(String Line2Icon) { this.Line2Icon = Line2Icon; }

    private String Line2Text;

    public String getLine2Text() { return this.Line2Text; }

    public void setLine2Text(String Line2Text) { this.Line2Text = Line2Text; }

    private String Line3Icon;

    public String getLine3Icon() { return this.Line3Icon; }

    public void setLine3Icon(String Line3Icon) { this.Line3Icon = Line3Icon; }

    private String Line3Text;

    public String getLine3Text() { return this.Line3Text; }

    public void setLine3Text(String Line3Text) { this.Line3Text = Line3Text; }

    private String Line4Icon;

    public String getLine4Icon() { return this.Line4Icon; }

    public void setLine4Icon(String Line4Icon) { this.Line4Icon = Line4Icon; }

    private String Line4Text;

    public String getLine4Text() { return this.Line4Text; }

    public void setLine4Text(String Line4Text) { this.Line4Text = Line4Text; }

    private String Line5Icon;

    public String getLine5Icon() { return this.Line5Icon; }

    public void setLine5Icon(String Line5Icon) { this.Line5Icon = Line5Icon; }

    private String Line5Text;

    public String getLine5Text() { return this.Line5Text; }

    public void setLine5Text(String Line5Text) { this.Line5Text = Line5Text; }

    private String Line6Icon;

    public String getLine6Icon() { return this.Line6Icon; }

    public void setLine6Icon(String Line6Icon) { this.Line6Icon = Line6Icon; }

    private String Line6Text;

    public String getLine6Text() { return this.Line6Text; }

    public void setLine6Text(String Line6Text) { this.Line6Text = Line6Text; }

}
