package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 9/22/2017.
 */

public class SchemeListModel {
    public String ID;
    public String Name;
    public String Image;
    public String SecondForm;

    public SchemeListModel() {
    }

    public SchemeListModel(String ID, String name, String image, String secondForm) {
        this.ID = ID;
        Name = name;
        Image = image;
        SecondForm = secondForm;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getSecondForm() {
        return SecondForm;
    }

    public void setSecondForm(String secondForm) {
        SecondForm = secondForm;
    }
}
