package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class TermsAndCondtitionActivity extends AppCompatActivity {

    WebView webView;
    Toolbar toolbar;
    TextView textView;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condtition);
        webView = (WebView) findViewById(R.id.terms_webview);
        toolbar = (Toolbar) findViewById(R.id.main_app_bar);

        textView = (TextView) toolbar.findViewById(R.id.tollbarText);
        textView.setText("Terms and Condition");
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        setSupportActionBar(toolbar);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://gwadar247.pk/terms.html");
        webView.setWebViewClient(new MyWebViewClient());


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(TermsAndCondtitionActivity.this, R.style.MyAlertDialogStyle);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please Wait");
        progressDialog.setCanceledOnTouchOutside(false);

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressDialog.show();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressDialog.dismiss();
            super.onPageFinished(view, url);
        }
    }

}
