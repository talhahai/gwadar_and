package com.example.shariqkhan.gawadar247;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.adapters.favouriteAdapter;
import com.example.shariqkhan.gawadar247.adapters.onTaskCompleted;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.BaseClass;
import com.example.shariqkhan.gawadar247.models.FavouriteModel;
import com.example.shariqkhan.gawadar247.models.NewsFeedModel;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.irozon.sneaker.Sneaker;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ShariqKhan on 10/7/2017.
 */

public class favouriteAdapter3 extends RecyclerView.Adapter<favouriteAdapter3.Recyclerholder> {


    private ArrayList<NewsFeedModel> arrayList = new ArrayList<>();

    public static String URL = "http://www.gwadar247.pk/api/addfavorite.php?";
    public String getid;
    public String gettoken;
    public String getPropertyid;
    public SharedPreferences prefs;
    public static NewsFeedModel data;
    public static int currentPosition;
    public Context context;
    String checkIfSuccess = "";
    public String LOGOUT_URL = "http://www.gwadar247.pk/api/logout.php";
    boolean fromRequest;

    public ProgressDialog progressDialog;


    public favouriteAdapter3(ArrayList<NewsFeedModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.fromRequest = false;
    }

    public favouriteAdapter3(ArrayList<NewsFeedModel> arrayList, Context context, boolean fromRequest) {
        this.arrayList = arrayList;
        this.context = context;
        this.fromRequest = fromRequest;

    }

    @Override
    public favouriteAdapter3.Recyclerholder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.for_notification_layout, parent, false);
        return new favouriteAdapter3.Recyclerholder(view);


    }

    @Override
    public void onBindViewHolder(final favouriteAdapter3.Recyclerholder holder, final int position) {

        prefs = context.getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        data =  arrayList.get(position);
        currentPosition = holder.getAdapterPosition();

        getid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);
        getPropertyid = data.getID();


        if(data.getIsRequested().equals("true")){
            holder.sendinquiry.setText("Delete Enquiry");
        }
        else{
            holder.sendinquiry.setText("Send Enquiry");
        }

        if(data.getIsFavorite().equals("true")){
            holder.addtocart.setText("Delete Favorite");
        }
        else{
            holder.addtocart.setText("Add to Favorite");
        }


        holder.sendinquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    data =  arrayList.get(position);
                    getPropertyid = data.getID();
                    if(data.getIsRequested().equals("true")){

                        new DeleteEnquiryTask(new onTaskCompleted() {
                            @Override
                            public void onTaskCompleted(String code) {
                                if (code.equals("000")) {
                                    holder.sendinquiry.setText("Send Enquiry");
                                    data.setIsRequested("false");
                                    notifyDataSetChanged();
                                } else {
                                    holder.sendinquiry.setText("Delete Enquiry");
                                }
                            }
                        }).execute();
                    }
                    else
                    {
                        SendInquiryTask task = new SendInquiryTask(new onTaskCompleted() {
                            @Override
                            public void onTaskCompleted(String code) {
                                if (code.equals("000")) {
                                    holder.sendinquiry.setText("Delete Enquiry");
                                    data.setIsRequested("true");
                                    notifyDataSetChanged();
                                } else {
                                    holder.sendinquiry.setText("Send Enquiry");
                                }
                            }
                        });
                        task.execute();
                    }

            }
        });



        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data =  arrayList.get(position);

                getPropertyid = data.getID();

                if(data.getIsFavorite().equals("true")){
                    new deleteFav(new onTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String code) {
                            if (code.equals("000")) {
                                holder.addtocart.setText("Add to Favorite");
                                data.setIsFavorite("false");
                                notifyDataSetChanged();
                            } else {
                                holder.addtocart.setText("Delete Favorite");
                            }
                        }
                    }).execute();
                }

                else {
                    Task task = new Task(new onTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String code) {
                            if (code.equals("000")) {
                                holder.addtocart.setText("Delete Favorite");
                                data.setIsFavorite("true");
                                notifyDataSetChanged();

                            } else {
                                holder.addtocart.setText("Add to Favorite");
                            }
                        }
                    });
                    task.execute();
                }

            }
        });

        holder.price.setText(data.getPrice());

        // line1text
        if(!data.getLine1Icon().equals("empty.png") && !data.getLine1Text().equals("")) {
            holder.line1text.setText(" " + data.getLine1Text());
            holder.line1text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine1Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line1text.setVisibility(View.GONE);

        // line2text
        if(!data.getLine2Icon().equals("empty.png") && !data.getLine2Text().equals("")) {
            holder.line2text.setText(" " + data.getLine2Text());
            holder.line2text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine2Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line2text.setVisibility(View.GONE);

        // line3text
        if(!data.getLine3Icon().equals("empty.png") && !data.getLine3Text().equals("")) {
            holder.line3text.setText(" " + data.getLine3Text());
            holder.line3text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine3Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line3text.setVisibility(View.GONE);

        // line4text
        if(!data.getLine4Icon().equals("empty.png") && !data.getLine4Text().equals("")) {
            holder.line4text.setText(" " + data.getLine4Text());
            holder.line4text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine4Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line4text.setVisibility(View.GONE);

        // line5text
        if(!data.getLine5Icon().equals("empty.png") && !data.getLine5Text().equals("")) {
            holder.line5text.setText(" " + data.getLine5Text());
            holder.line5text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine5Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line5text.setVisibility(View.GONE);

        // line6text
        if(!data.getLine6Icon().equals("empty.png") && !data.getLine6Text().equals("")) {
            holder.line6text.setText(" " + data.getLine6Text());
            holder.line6text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine6Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line6text.setVisibility(View.GONE);



        holder.ratingBar.setRating(Float.parseFloat(data.getRating()));
        holder.ratingBar.setEnabled(false);
        Picasso.with(holder.circleImageView.getContext()).load(data.getImage()).into(holder.circleImageView);
        //holder.circleImageView.setImageResource(R.drawable.circularimage);



        float scale = holder.addtocart.getContext().getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (8 * scale + 0.5f);
        holder.addtocart.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);


        if (position % 2 == 0)
            holder.relativeLayout.setBackgroundColor(Color.rgb(242, 242, 242));


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public static class Recyclerholder extends RecyclerView.ViewHolder {

        public TextView price, line1text, line2text, line3text, line4text, line5text, line6text;
        public Button sendinquiry, addtocart;
        CircleImageView circleImageView;
        SimpleRatingBar ratingBar;
        View view;
        RelativeLayout relativeLayout;

        public Recyclerholder(View itemView) {
            super(itemView);
            view = itemView;

            relativeLayout = (RelativeLayout) view.findViewById(R.id.use_for_color);

            price = (TextView) itemView.findViewById(R.id.amount);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.profile_image);
            ratingBar = (SimpleRatingBar) itemView.findViewById(R.id.rating);

            sendinquiry = (Button) itemView.findViewById(R.id.sendenquiry);
            addtocart = (Button) itemView.findViewById(R.id.add_to_cart);

            line1text = (TextView) itemView.findViewById(R.id.line1text);
            line2text = (TextView) itemView.findViewById(R.id.line2text);
            line3text = (TextView) itemView.findViewById(R.id.line3text);
            line4text = (TextView) itemView.findViewById(R.id.line4text);
            line5text = (TextView) itemView.findViewById(R.id.line5text);
            line6text = (TextView) itemView.findViewById(R.id.line6text);

        }
    }

    private class Task extends AsyncTask<Object, Object, String> {

        onTaskCompleted onTaskCompleted;

        public Task(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }

        @Override
        protected String doInBackground(Object... voids) {

            String favURL = URL + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                this.onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {

                    progressDialog.dismiss();
                    Sneaker.with((Activity) context)
                            .setTitle("Added!")
                            .setDuration(5000)
                            .setMessage("Successfully Added!")
                            .sneakSuccess();





                } else if (getCode.equals("802")) {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(5000)
                            .setMessage("Re Login.")
                            .sneakError();
                    Task2 task = new Task2();
                    task.execute();

                } else if (getCode.equals("001")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("This Property Has been Sold!")
                            .sneakError();
                    progressDialog.dismiss();
                } else if (getCode.equals("002")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Its your own property")
                            .sneakError();
                    progressDialog.dismiss();
                } else {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Some Exception Occured!")
                            .sneakError();
                    progressDialog.dismiss();
                }


            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Adding to Favorite");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class deleteFav extends AsyncTask<Object, Object, String> {

        public onTaskCompleted onTaskCompleted;

        public deleteFav(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }
        @Override
        protected String doInBackground(Object... voids) {

            String favURL = "http://www.gwadar247.pk/api/deletefavorite.php?" + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                this.onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {

                    progressDialog.dismiss();
                    Sneaker.with((Activity) context)
                            .setTitle("Deleted!")
                            .setDuration(5000)
                            .setMessage("Successfully Deleted.")
                            .sneakSuccess();

                } else if (getCode.equals("802")) {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(5000)
                            .setMessage("Re Login.")
                            .sneakError();
                    Task2 task = new Task2();
                    task.execute();

                } else if (getCode.equals("001")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Favorite not deleted")
                            .sneakError();
                    progressDialog.dismiss();
                } else if (getCode.equals("002")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Its your own property")
                            .sneakError();
                    progressDialog.dismiss();
                } else {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Some Exception Occured!")
                            .sneakError();
                    progressDialog.dismiss();
                }


            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Deleting");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }



    private class SendInquiryTask extends AsyncTask<Object, Object, String> {

        onTaskCompleted onTaskCompleted;

        public SendInquiryTask(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }

        @Override
        protected String doInBackground(Object... voids) {

            String favURL = "http://www.gwadar247.pk/api/sendrequest.php?" + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");
                checkIfSuccess = getCode;

                this.onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {

                    Log.e("EnquiryIssue", "Enquiry Sent");
                    progressDialog.dismiss();
                    Sneaker.with((Activity) context)
                            .setTitle("Successfull")
                            .setDuration(5000)
                            .setMessage("Enquiry Sent")
                            .sneakSuccess();


                } else if (getCode.equals("802")) {
                    Toast.makeText(context, "Login Session Expired!", Toast.LENGTH_SHORT).show();
                    Task2 task = new Task2();
                    task.execute();

                } else {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(5000)
                            .setMessage("You can't send enquiry to own property.")
                            .sneakError();
                    progressDialog.dismiss();


                }

            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("EnquiryIssue", e.getMessage());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Sending Enquiry");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    public class DeleteEnquiryTask extends AsyncTask<Object, Object, String> {

        public onTaskCompleted onTaskCompleted;

        public DeleteEnquiryTask(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }
        @Override
        protected String doInBackground(Object... voids) {

            String favURL = "http://www.gwadar247.pk/api/deleterequest.php?" + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {
                    Log.e("EnquiryIssue", "Enquiry Deleted");
                    progressDialog.dismiss();
                    Sneaker.with((Activity) context)
                            .setTitle("Successfull")
                            .setDuration(4000)
                            .setMessage("Enquiry Deleted")
                            .sneakSuccess();
                    Toast.makeText(context, "Enquiry Deleted!", Toast.LENGTH_SHORT).show();
                    if(fromRequest){
                        int position = arrayList.indexOf(data);
                        arrayList.remove(position);
                        notifyDataSetChanged();
                    }

                } else if (getCode.equals("802")) {
                    Toast.makeText(context, "Login Session Expired!", Toast.LENGTH_SHORT).show();
                    Task2 task = new Task2();
                    task.execute();

                } else {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(4000)
                            .setMessage("Failed to Delete Enquiry")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("EnquiryIssue", e.getMessage());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Deleting Enquiry");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }




    private class Task2 extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {


            String url = LOGOUT_URL + "?userid=" + getid + "&" + "token=" + gettoken;
            //  Log.e("logout", url);


            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);
                String getCode = jsonObject.getString("ErrorCode");
                if (getCode.equals("000")) {
                    Toast.makeText(context, "Re Login!", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);


                } else if (getCode.equals("802")) {

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();

                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(5000)
                            .setMessage("Login Again")
                            .sneakError();

                    progressDialog.dismiss();
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                } else {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(5000)
                            .setMessage("Something Went Wrong! Please Relogin.")
                            .sneakWarning();
                    progressDialog.dismiss();
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                }

            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Logging Out");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

}


