package com.example.shariqkhan.gawadar247;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.shariqkhan.gawadar247.DaniyalClasses.Data;
import com.example.shariqkhan.gawadar247.DaniyalClasses.Recycleradapter;
import com.example.shariqkhan.gawadar247.Utils.NavViewHelper;
import com.example.shariqkhan.gawadar247.adapters.FavouritePagerAdapter;
import com.example.shariqkhan.gawadar247.adapters.favouriteAdapter;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.FavouriteModel;
import com.example.shariqkhan.gawadar247.models.SchemeListModel;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Favourite extends AppCompatActivity {
    Toolbar toolbar;
    private BottomNavigationViewEx bottomNavigationViewEx;
    TextView toolbarText;

    ViewPager viewPager;
    public TabLayout tabLayout;
    int Act_Num = 1;
    static Favourite favRef = null;
    public FavouritePagerAdapter sectionPagerAdapter;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    favRef = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);
        favRef = this;
        overridePendingTransition(0, 0);
        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);
        bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottom_nav_bar);
        NavViewHelper.modifyBottomNavBarEx(bottomNavigationViewEx);
        NavViewHelper.enableNavigation(Favourite.this, bottomNavigationViewEx);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(Act_Num);
        menuItem.setChecked(true);


        tabLayout = (TabLayout) findViewById(R.id.main_tab);
        toolbarText = (TextView) toolbar.findViewById(R.id.tollbarText);
        toolbarText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.main_viewpager);
        sectionPagerAdapter = new FavouritePagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(sectionPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
