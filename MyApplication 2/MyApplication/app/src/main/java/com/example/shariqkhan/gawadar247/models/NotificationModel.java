package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 10/12/2017.
 */

public class NotificationModel {

   public String NotifyTime;
   public String Id;
   public String NotificationType;
   public String IsSeen;
   public String Response;
   public String RequestID;
   public String PropertID;
   public String PropertyType;
   public String PlotNo;
   public String Type;
   public String IsResponded;
   public String Message;

   public NotificationModel() {
   }

   public String getNotifyTime() {
      return NotifyTime;
   }

   public void setNotifyTime(String notifyTime) {
      NotifyTime = notifyTime;
   }

   public String getId() {
      return Id;
   }

   public void setId(String id) {
      Id = id;
   }

   public String getNotificationType() {
      return NotificationType;
   }

   public void setNotificationType(String notificationType) {
      NotificationType = notificationType;
   }

   public String getIsSeen() {
      return IsSeen;
   }

   public void setIsSeen(String isSeen) {
      IsSeen = isSeen;
   }

   public String getResponse() {
      return Response;
   }

   public void setResponse(String response) {
      Response = response;
   }

   public String getRequestID() {
      return RequestID;
   }

   public void setRequestID(String requestID) {
      RequestID = requestID;
   }

   public String getPropertID() {
      return PropertID;
   }

   public void setPropertID(String propertID) {
      PropertID = propertID;
   }

   public String getPropertyType() {
      return PropertyType;
   }

   public void setPropertyType(String propertyType) {
      PropertyType = propertyType;
   }

   public String getPlotNo() {
      return PlotNo;
   }

   public void setPlotNo(String plotNo) {
      PlotNo = plotNo;
   }

   public String getType() {
      return Type;
   }

   public void setType(String type) {
      Type = type;
   }

   public String getIsResponded() {
      return IsResponded;
   }

   public void setIsResponded(String isResponded) {
      IsResponded = isResponded;
   }

   public String getMessage() {
      return Message;
   }

   public void setMessage(String message) {
      Message = message;
   }
}
