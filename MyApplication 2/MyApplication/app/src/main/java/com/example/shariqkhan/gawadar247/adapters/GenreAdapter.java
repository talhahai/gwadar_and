package com.example.shariqkhan.gawadar247.adapters;

/**
 * Created by ShariqKhan on 8/30/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.R;
import com.example.shariqkhan.gawadar247.SearchActivity;
import com.example.shariqkhan.gawadar247.SearchCallback;
import com.example.shariqkhan.gawadar247.SearchReultActivity;
import com.example.shariqkhan.gawadar247.models.Artist;
import com.example.shariqkhan.gawadar247.models.Genre;
import com.example.shariqkhan.gawadar247.viewholders.ArtistViewHolder;
import com.example.shariqkhan.gawadar247.viewholders.GenreViewHolder;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Brad on 12/18/2016.
 */

public class GenreAdapter extends ExpandableRecyclerViewAdapter<GenreViewHolder, ArtistViewHolder> {
    public static FloatingActionButton floatingActionButton;
    List<Genre> groups = new ArrayList<>();
    public static ArrayList<String> sendList = new ArrayList<>();
    public SearchCallback searchCallback;
    int check =0;
    List<? extends ExpandableGroup> expandableGroups;



    public GenreAdapter(List<? extends ExpandableGroup> groups, FloatingActionButton floatingActionButton, SearchCallback searchCallback) {
        super(groups);
        this.searchCallback = searchCallback;
        GenreAdapter.floatingActionButton = floatingActionButton;
        this.expandableGroups = groups;
    }



    @Override
    public GenreViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_genre, parent, false);
        return new GenreViewHolder(view);
    }

    @Override
    public ArtistViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_artist, parent, false);
        return new ArtistViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(final ArtistViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final Artist artist = (Artist) group.getItems().get(childIndex);

        holder.setArtistName(artist.getName());


        if(!sendList.contains(artist.getId()))
            holder.artistImage.setImageResource(R.drawable.ic_ucheck);
        else
            holder.artistImage.setImageResource(R.drawable.ic_check);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(sendList.contains(artist.getId())){
                    holder.artistImage.setImageResource(R.drawable.ic_ucheck);
                    sendList.remove(artist.getId());
                    if(sendList.size()== 0){
                        floatingActionButton.setVisibility(View.GONE);
                    }
                }
                else{
                    holder.artistImage.setImageResource(R.drawable.ic_check);
                    sendList.add(artist.getId());
                    floatingActionButton.setVisibility(View.VISIBLE);
                }


            }
        });
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchCallback.onCompleted(sendList);
                check = 1;
//                Set<String> set = new HashSet<>();
//                for (int i = 0; i <sendList.size() ; i++) {
//                    set.add(sendList.get(i));
//                }
//                sendList.clear();
//                sendList.addAll(set);
                Log.e("Passing List", String.valueOf(sendList));
//                Set<String> s = new HashSet<String>();
//                s.addAll(sendList);
//                sendList.clear();
//                sendList.addAll(s);
                //intent.putStringArrayListExtra("Values", sendList);

                //context.overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_left);
                //context.startActivity(intent);
                //context.finish();
            }
        });
    }
    @Override
    public void onBindGroupViewHolder(final GenreViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGenreName(group.getTitle());


    }


}

