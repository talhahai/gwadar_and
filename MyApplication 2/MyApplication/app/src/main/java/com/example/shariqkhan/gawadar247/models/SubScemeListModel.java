package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 9/22/2017.
 */

public class SubScemeListModel {
    public String ID;
    public String Name;
    public String Image;
    public String Popular;
    public String AreaUnit;
    public String LayerName;

    public SubScemeListModel() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getPopular() {
        return Popular;
    }

    public void setPopular(String popular) {
        Popular = popular;
    }

    public String getAreaUnit() {
        return AreaUnit;
    }

    public void setAreaUnit(String areaUnit) {
        AreaUnit = areaUnit;
    }

    public String getLayerName() {
        return LayerName;
    }

    public void setLayerName(String layerName) {
        LayerName = layerName;
    }
}
