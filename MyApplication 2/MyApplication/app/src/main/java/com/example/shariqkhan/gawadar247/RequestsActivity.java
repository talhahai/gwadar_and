package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;


public class RequestsActivity extends AppCompatActivity {
    public static void giveInstance() {

        Intent intent = new Intent(ct, RequestsActivity.class);
        ct.finish();
        Log.e("insideInstance", "");
        ct.startActivity(intent);


    }


    TextView textView;

    SharedPreferences prefs;
    String getid;
    String gettoken;
    public static int activityCheck  =0;

    Toolbar toolbar;

    TextView toolbarText;

    ViewPager viewPager;
    public static RequestsActivity ct;
    public TabLayout tabLayout;

    public MyRequestPagerAdapter sectionPagerAdapter;
    ProgressDialog progressDialog;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        overridePendingTransition(0,0);
        setSupportActionBar(toolbar);
        ct = this;
        toolbarText = (TextView) toolbar.findViewById(R.id.tollbarText);
        tabLayout = (TabLayout) findViewById(R.id.main_tab);
        toolbarText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        viewPager = (ViewPager) findViewById(R.id.main_viewpager);
        sectionPagerAdapter = new MyRequestPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(sectionPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
}
