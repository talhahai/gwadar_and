package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 9/22/2017.
 */

public class Division2ListModel {
public String ID;
    public String Name;

    public Division2ListModel() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
