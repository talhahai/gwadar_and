package com.example.shariqkhan.gawadar247;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v13.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.LoginModel;
import com.google.gson.Gson;
import com.irozon.sneaker.Sneaker;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

import org.androidannotations.annotations.HttpsClient;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static android.graphics.Color.TRANSPARENT;
import static com.example.shariqkhan.gawadar247.FormActivity.form;

public class LoginActivity extends AppCompatActivity {

    private EditText phone;
    private EditText password;
    String phNo;
    String userpassword;
    Button signUp, forgetPassword;
    private ProgressDialog progressDialog;
    public Dialog progressDialog2;
    public static String deviceToken = "";
    public static String uid = "";
    ProgressWheel pg;
    Button createaccount;
    String contactNo;


    RelativeLayout relative;
    public static String BASE_URL = "http://www.gwadar247.pk/api/login.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        createaccount = (Button) findViewById(R.id.createaccount);
        forgetPassword = (Button) findViewById(R.id.forget);
        phone = (EditText) findViewById(R.id.user_name_signup);
        password = (EditText) findViewById(R.id.password);
        relative = (RelativeLayout) findViewById(R.id.activity_login);
        if (MainActivity.ct != null) MainActivity.ct.finish();
        signUp = (Button) findViewById(R.id.Signin);

        createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactMeAsync async = new ContactMeAsync();
                async.execute();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phNo = phone.getText().toString();
                userpassword = password.getText().toString();


                Task task = new Task();
                task.execute(phNo, userpassword);


            }
        });
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactMeAsync async = new ContactMeAsync();
                async.execute();
            }
        });

    }

    private class Task extends AsyncTask<String, Void, String> {
        String stream = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Logging In");

            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String[] params) {

            String getResponse = getJson();
            stream = getResponse;

            return stream;

        }

        private String getJson() {
            HttpClient httpClient = new DefaultHttpClient();


            HttpPost post = new HttpPost(BASE_URL);

            List<NameValuePair> parameters = new ArrayList<>();
            parameters.add(new BasicNameValuePair("phone", phNo));
            parameters.add(new BasicNameValuePair("password", userpassword));

            StringBuilder buffer = new StringBuilder();

            try {
                // Log.e("Insidegetjson", "insidetry");
                UrlEncodedFormEntity encoded = new UrlEncodedFormEntity(parameters);
                post.setEntity(encoded);
                HttpResponse response = httpClient.execute(post);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String Line = "";

                while ((Line = reader.readLine()) != null) {
                    Log.e("buffer", buffer.toString());
                    buffer.append(Line);

                }
                reader.close();

            } catch (Exception o) {
                Log.e("Error", o.getMessage());

            }
            return buffer.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonobj;
            if (s != null) {
                try {
                    jsonobj = new JSONObject(s);
                    Gson gson = new Gson();
                    LoginModel model = gson.fromJson(s, LoginModel.class);

                    String errorCode = jsonobj.getString("ErrorCode");

                    LoginModel loginModel = new LoginModel();

                    deviceToken = jsonobj.getString("Token");

                    uid = jsonobj.getString("UserID");
                    SharedPreferences.Editor editor = getSharedPreferences("SharedPreferences", MODE_PRIVATE).edit();

                    editor.putString("id", uid);
                    editor.putString("token", deviceToken);
                    editor.apply();

                    loginModel.UserID = uid;
                    loginModel.Token = deviceToken;
                    if (errorCode.equals("000")) {
                        Sneaker.with(LoginActivity.this)
                                .setDuration(2000)
                                .setMessage("Login Successfull")
                                .sneakSuccess();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        phone.setText("");
                        password.setText("");


                    } else {
                        Sneaker.with(LoginActivity.this)
                                .setTitle("Error!")
                                .setDuration(3000)
                                .setMessage("Something went wrong or Invalid credentials!")
                                .sneakError();

                        password.setText("");


                    }
                } catch (JSONException e) {
                    Sneaker.with(LoginActivity.this)
                            .setTitle("Error! Can not login")
                            .setDuration(5000)
                            .setMessage("Incorrect data or No internet connection")
                            .sneakError();
                    password.setText("");
                    progressDialog.dismiss();
                }


            }

            progressDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.e("start","start");
        SharedPreferences prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        String restoredPh = prefs.getString("id", null);
        String restoredPassword = prefs.getString("token", null);
        if (restoredPh != null || restoredPassword != null) {
            //       Log.e("start", restoredPh);
            String url = "http://www.gwadar247.pk/api/newsfeed2.php";
            Log.e("url", url);
            String response = null;
            boolean InternetError = true;
            try {
                response = getHttpData.getData(url);
                InternetError = false;
            }
            catch (Exception e) {
            }

//            if (InternetError == true)
//            {
//                Sneaker.with(LoginActivity.this)
//                        .setTitle("No internet connection!")
//                        .setDuration(5000)
//                        .setMessage("No internet connection. Retry with stable connection")
//                        .sneakError();
//            }
//            else
               {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, R.anim.slide_out_right);
    }

    private class ContactMeAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... voids) {
            String url = "http:www.gwadar247.pk/api/getcontact.php";


            Log.e("url", url);

            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                contactNo = jsonObject.getString("Phone");
                callPhoneNumber();


                progressDialog2.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = new ProgressDialog(LoginActivity.this, R.style.MyAlertDialogStyle);
//            progressDialog.setTitle("Calling");
//            progressDialog.setMessage("Please Wait");
//
//            progressDialog.setCanceledOnTouchOutside(false);
//            progressDialog.show();

      progressDialog2=  new LovelyProgressDialog(LoginActivity.this)
                .setTopColorRes(R.color.darkgreen)
                .setTitle("Calling")
                .setMessage("Please Wait")
                .setIcon(R.drawable.call)
                .setCancelable(false)
                .show();
        }


    }

    public void callPhoneNumber()
    {
        try
        {
            if(Build.VERSION.SDK_INT > 22)
            {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling

                    ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 101);

                    return;
                }

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + contactNo));
                startActivity(callIntent);

            }
            else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + contactNo));
                startActivity(callIntent);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults)
    {
        if(requestCode == 101)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                callPhoneNumber();
            }
            else
            {
                Log.e("NotGranted", "Permission not Granted");
            }
        }
    }

}
