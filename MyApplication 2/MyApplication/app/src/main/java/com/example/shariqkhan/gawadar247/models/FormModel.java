package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 9/30/2017.
 */

public class FormModel {

    public String ID;
    public String Name;
    public String HasLayer;
    public String LayerName;

    public FormModel() {

    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHasLayer() {
        return HasLayer;
    }

    public void setHasLayer(String hasLayer) {
        HasLayer = hasLayer;
    }

    public String getLayerName() {
        return LayerName;
    }

    public void setLayerName(String layerName) {
        LayerName = layerName;
    }
}
