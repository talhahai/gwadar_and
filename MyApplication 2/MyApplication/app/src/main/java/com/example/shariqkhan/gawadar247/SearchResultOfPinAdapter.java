package com.example.shariqkhan.gawadar247;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ShariqKhan on 10/23/2017.
 */

public class SearchResultOfPinAdapter extends FragmentPagerAdapter {
    public SearchResultOfPinAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                SalesFragOfPins wanted = new SalesFragOfPins();
                return wanted;
            case 1:
                WantedFragOfPins sales = new WantedFragOfPins();
                return sales;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Sales";
            case 1:
                return "Wanted";
            default:
                return null;
        }

    }
}
