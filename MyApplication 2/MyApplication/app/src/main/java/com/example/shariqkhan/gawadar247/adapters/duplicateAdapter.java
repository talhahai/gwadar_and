package com.example.shariqkhan.gawadar247.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.R;


public class duplicateAdapter extends BaseAdapter {

    Context context;
    String[] list;
    public duplicateAdapter(Context context, String[] list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context
                .getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            view = inflater.inflate(
                    R.layout.duplicate_plotsview, null);
            TextView textView = (TextView)view.findViewById(R.id.text);
            textView.setText(list[i]);
        }

        return  view;


    }
}
