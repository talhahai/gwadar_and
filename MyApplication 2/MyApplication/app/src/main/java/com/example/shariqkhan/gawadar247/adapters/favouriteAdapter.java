package com.example.shariqkhan.gawadar247.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.DaniyalClasses.Sales;
import com.example.shariqkhan.gawadar247.LoginActivity;
import com.example.shariqkhan.gawadar247.MainActivity;
import com.example.shariqkhan.gawadar247.R;
import com.example.shariqkhan.gawadar247.SearchReultActivity;
import com.example.shariqkhan.gawadar247.favouriteAdapter2;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.BaseClass;
import com.example.shariqkhan.gawadar247.models.FavouriteModel;
import com.example.shariqkhan.gawadar247.models.NewsFeedModel;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.irozon.sneaker.Sneaker;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ShariqKhan on 9/27/2017.
 */
public class favouriteAdapter extends RecyclerView.Adapter<favouriteAdapter.Recyclerholder>{

    private ArrayList<NewsFeedModel> arrayList = new ArrayList<>();

    public static String URL = "http://www.gwadar247.pk/api/addfavorite.php?";
    public static String deleteFavURL = "http://www.gwadar247.pk/api/deletefavorite.php?";
    public String getid;
    public String gettoken;
    public String getPropertyid;
    public SharedPreferences prefs;
    public Context context;
    public String CheckCodeForButtonFav;
    public String CheckCodeForButtonInq;
    public String LOGOUT_URL = "http://www.gwadar247.pk/api/logout.php";
    int checkForRestart;
    AdapterInterface adapterInterfaceCallback;
    public NewsFeedModel data;

    public ProgressDialog progressDialog;

    public favouriteAdapter(ArrayList<NewsFeedModel> arrayList, Context context, int checkForRestart, AdapterInterface adapterInterface) {
        this.arrayList = arrayList;
        this.context = context;
        this.checkForRestart = checkForRestart;
        this.adapterInterfaceCallback = adapterInterface;
    }

    public Recyclerholder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.for_notification_layout, parent, false);
        return new favouriteAdapter.Recyclerholder(view);
    }

    @Override
    public void onBindViewHolder(final favouriteAdapter.Recyclerholder holder, final int position) {

        prefs = context.getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        data = arrayList.get(position);

        getid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);
        getPropertyid = data.getID();

        Log.d("ididid", getPropertyid);

        if (data.getIsRequested().equals("true")) {
            holder.sendinquiry.setText("Delete Enquiry");

        } else {
            holder.sendinquiry.setText("Send Enquiry");
        }

        if(data.getIsFavorite().equals("true")){
            holder.addtocart.setText("Delete Favorite");
        }
        else{
            holder.addtocart.setText("Add to Favorite");
        }


        holder.sendinquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data = arrayList.get(position);
                if (data.getIsRequested().equals("true")) {
                    getPropertyid = data.getID();
                    new DeleteEnquiryTask(new onTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String code) {
                            if (code.equals("000")) {
                                holder.sendinquiry.setText("Send Enquiry");
                                data.setIsRequested("false");
                                //adapterInterfaceCallback.onRefresh();
                            } else {
                                holder.sendinquiry.setText("Delete Enquiry");
                                //adapterInterfaceCallback.onRefresh();
                            }
                        }
                    }).execute();

                } else {
                    getPropertyid = data.getID();
                    new SendInquiryTask(new onTaskCompleted(){
                        @Override
                        public void onTaskCompleted(String code) {
                            if (code.equals("000")) {
                                holder.sendinquiry.setText("Delete Enquiry");
                                data.setIsRequested("true");
                                //adapterInterfaceCallback.onRefresh();
                            } else {
                                holder.sendinquiry.setText("Send Enquiry");
                                //adapterInterfaceCallback.onRefresh();
                            }
                        }
                    }).execute();

                }
            }
        });


        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data = arrayList.get(position);
                if (!data.getIsFavorite().equals("true")) {

                    getPropertyid = data.getID();
                    new Task(new onTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String code) {
                            if (code.equals("000")) {
                                holder.addtocart.setText("Delete Favorite");
                                data.setIsFavorite("true");
                            } else {
                                holder.addtocart.setText("Add to Favorite");
                            }
                        }
                    }).execute();

                } else {
                    getPropertyid = data.getID();
                    new deleteFav(new onTaskCompleted() {
                        @Override
                        public void onTaskCompleted(String code) {
                            if (code.equals("000")) {
                                holder.addtocart.setText("Add to Favorite");
                                data.setIsFavorite("false");
                            } else {
                                holder.addtocart.setText("Delete Favorite");
                            }
                        }
                    }).execute();
//                    Sneaker.with((Activity) context)
//                            .setDuration(5000)
//                            .setMessage("Already Favourite")
//                            .sneakWarning();
//                    holder.addtocart.setText("Favorite");
                }
            }
        });

       /* if(data.getArea().equals("") || data.getArea() == null){
            holder.area.setVisibility(View.GONE);
        }
        else
            holder.area.setText(" "+data.getArea());

        holder.price.setText(" "+data.getPrice());

        holder.subscheme.setText(" "+data.SubSchemeName);
        String plottype = data.getPlotType();
        if (plottype.equals("Recreational")) {
            holder.scheme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.recreational_3x, 0, 0, 0);
        } else if (plottype.equals("Industrial")) {
            holder.scheme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.industrial_3x, 0, 0, 0);
        } else if (plottype.equals("Residential")) {
            holder.scheme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.residential_3x, 0, 0, 0);
        } else if (plottype.equals("Commercial")) {
            holder.scheme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.commercial_3x, 0, 0, 0);
        } else if (plottype.equals("HI-RISE")) {
            holder.scheme.setCompoundDrawablesWithIntrinsicBounds(R.drawable.hi_rise_3x, 0, 0, 0);
        }

        if(plottype.equals("") || plottype == null){
            holder.scheme.setVisibility(View.GONE);
        }
        else{
            holder.scheme.setText(" "+plottype);
        }
*/

        holder.ratingBar.setRating(Float.parseFloat(data.getRating()));

        holder.price.setText(" "+data.getPrice());

        Picasso.with(holder.circleImageView.getContext()).load(data.getImage()).into(holder.circleImageView);

        /*//holder.circleImageView.setImageResource(R.drawable.circularimage);
        holder.phase.setText(" "+data.getLayer1Name());
        if (!data.getLayer2Name().equals(""))
        {
            holder.phase2.setText(" "+data.getLayer2Name());
        }else
            {
                holder.phase2.setVisibility(View.GONE);
            }

        if (data.getPlotNo().equals("")){holder.plotno.setVisibility(View.GONE);}else{holder.plotno.setText(" "+data.getPlotNo());}

*/

        // line1text
        if(!data.getLine1Icon().equals("empty.png") && !data.getLine1Text().equals("")) {
            holder.line1text.setText(" " + data.getLine1Text());
            holder.line1text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine1Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line1text.setVisibility(View.GONE);

        // line2text
        if(!data.getLine2Icon().equals("empty.png") && !data.getLine2Text().equals("")) {
            holder.line2text.setText(" " + data.getLine2Text());
            holder.line2text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine2Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line2text.setVisibility(View.GONE);

        // line3text
        if(!data.getLine3Icon().equals("empty.png") && !data.getLine3Text().equals("")) {
            holder.line3text.setText(" " + data.getLine3Text());
            holder.line3text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine3Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line3text.setVisibility(View.GONE);

        // line4text
        if(!data.getLine4Icon().equals("empty.png") && !data.getLine4Text().equals("")) {
            holder.line4text.setText(" " + data.getLine4Text());
            holder.line4text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine4Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line4text.setVisibility(View.GONE);

        // line5text
        if(!data.getLine5Icon().equals("empty.png") && !data.getLine5Text().equals("")) {
            holder.line5text.setText(" " + data.getLine5Text());
            holder.line5text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine5Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line5text.setVisibility(View.GONE);

        // line6text
        if(!data.getLine6Icon().equals("empty.png") && !data.getLine6Text().equals("")) {
            holder.line6text.setText(" " + data.getLine6Text());
            holder.line6text.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(data.getLine6Icon().split("\\.")[0], "drawable", context.getPackageName()), 0, 0, 0);
        }
        else
            holder.line6text.setVisibility(View.GONE);




        float scale = holder.addtocart.getContext().getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (8 * scale + 0.5f);
        holder.addtocart.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);


        if (position % 2 == 0) {
            holder.relativeLayout.setBackgroundColor(Color.rgb(242, 242, 242));
        } else {
            holder.relativeLayout.setBackgroundColor(Color.rgb(255, 255, 255));
        }


    }

    @Override
    public int getItemCount() {

        return arrayList.size();
    }

    public static class Recyclerholder extends RecyclerView.ViewHolder {

        public TextView price, line1text, line2text, line3text, line4text, line5text, line6text;
        public Button sendinquiry, addtocart;
        CircleImageView circleImageView;
        SimpleRatingBar ratingBar;
        View view;
        RelativeLayout relativeLayout;

        public Recyclerholder(View itemView) {
            super(itemView);
            view = itemView;

            relativeLayout = (RelativeLayout) view.findViewById(R.id.use_for_color);

            price = (TextView) itemView.findViewById(R.id.amount);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.profile_image);
            ratingBar = (SimpleRatingBar) itemView.findViewById(R.id.rating);

            sendinquiry = (Button) itemView.findViewById(R.id.sendenquiry);
            addtocart = (Button) itemView.findViewById(R.id.add_to_cart);

            line1text = (TextView) itemView.findViewById(R.id.line1text);
            line2text = (TextView) itemView.findViewById(R.id.line2text);
            line3text = (TextView) itemView.findViewById(R.id.line3text);
            line4text = (TextView) itemView.findViewById(R.id.line4text);
            line5text = (TextView) itemView.findViewById(R.id.line5text);
            line6text = (TextView) itemView.findViewById(R.id.line6text);


        }
    }

    public class SendInquiryTask extends AsyncTask<Object, Object, String> {

        public  onTaskCompleted onTaskCompleted;

        public  SendInquiryTask(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }
        @Override
        protected String doInBackground(Object... voids) {

            String favURL = "http://www.gwadar247.pk/api/sendrequest.php?" + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");
                CheckCodeForButtonInq = getCode;

                onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {

                    Log.e("EnquiryIssue", "Enquiry Sent");
                    progressDialog.dismiss();
                    CheckCodeForButtonInq = getCode;
                    Sneaker.with((Activity) context)
                            .setTitle("Successfull")
                            .setDuration(4000)
                            .setMessage("Enquiry Sent")
                            .sneakSuccess();
                    Toast.makeText(context, "Enquiry Sent!", Toast.LENGTH_LONG).show();
                        if (checkForRestart == 1)
                        {

                        }
                        else
                            {
                                //SearchReultActivity.giveInstance();
                            }



                } else if (getCode.equals("802")) {
                    CheckCodeForButtonInq = "802";
                    Toast.makeText(context, "Login Session Expired!", Toast.LENGTH_SHORT).show();
                    Task2 task = new Task2();
                    task.execute();

                } else {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(4000)
                            .setMessage("You can't send enquiry to own property.")
                                .sneakError();
                    progressDialog.dismiss();
                    CheckCodeForButtonInq = getCode;


                }

            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("EnquiryIssue", e.getMessage());
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Sending Enquiry");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    public class DeleteEnquiryTask extends AsyncTask<Object, Object, String> {

        public onTaskCompleted onTaskCompleted;

        public DeleteEnquiryTask(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }
        @Override
        protected String doInBackground(Object... voids) {

            String favURL = "http://www.gwadar247.pk/api/deleterequest.php?" + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {
                    //    Recyclerholder.addtocart.setText("Favourite");
                    CheckCodeForButtonInq = getCode;
                    Log.e("EnquiryIssue", "Enquiry Deleted");
                    progressDialog.dismiss();
                    Sneaker.with((Activity) context)
                            .setTitle("Successfull")
                            .setDuration(4000)
                            .setMessage("Enquiry Deleted")
                            .sneakSuccess();
                    Toast.makeText(context, "Enquiry Deleted!", Toast.LENGTH_SHORT).show();
                    if (checkForRestart == 1)
                    {
                        //MainActivity.giveInstance();
                    }else
                    {
                        //SearchReultActivity.giveInstance();
                    }



                } else if (getCode.equals("802")) {
                    CheckCodeForButtonInq = getCode;
                    Toast.makeText(context, "Login Session Expired!", Toast.LENGTH_SHORT).show();
                    Task2 task = new Task2();
                    task.execute();

                } else {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(4000)
                            .setMessage("Failed to Delete Enquiry")
                            .sneakError();
                    progressDialog.dismiss();
                    CheckCodeForButtonInq = getCode;

                }

            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("EnquiryIssue", e.getMessage());
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Deleting Enquiry");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    private class Task extends AsyncTask<Object, Object, String> {

        public onTaskCompleted onTaskCompleted;
        public Task(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }
        @Override
        protected String doInBackground(Object... voids) {

            String favURL = URL + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");
                CheckCodeForButtonFav = getCode;
                onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {
                    //     Recyclerholder.addtocart.setText("Favourite");

                    Log.e("AddedtoFavourites", "Added to Favourites");
                    progressDialog.dismiss();
                    Sneaker.with((Activity) context)
                            .setTitle("Successfull")
                            .setDuration(4000)
                            .setMessage("Added to Favorites")
                            .sneakSuccess();
                    Toast.makeText(context, "Added to Favorites!", Toast.LENGTH_SHORT).show();
                    if (checkForRestart == 1)
                    {

                    }else
                    {
                        //SearchReultActivity.giveInstance();
                    }

                } else if (getCode.equals("802")) {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(4000)
                            .setMessage("Re Login.")
                            .sneakError();
                    Task2 task = new Task2();
                    task.execute();

                } else if (getCode.equals("001")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(4000)
                            .setMessage("You can't favorite your own property.")
                            .sneakError();
                    progressDialog.dismiss();
                } else if (getCode.equals("002")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(4000)
                            .setMessage("You can't favorite your own property.")
                            .sneakError();
                    progressDialog.dismiss();
                } else {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(4000)
                            .setMessage("Some Exception Occured!")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Adding to Favorite");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class deleteFav extends AsyncTask<Object, Object, String> {

        public onTaskCompleted onTaskCompleted;

        public deleteFav(onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
        }
        @Override
        protected String doInBackground(Object... voids) {

            String favURL = deleteFavURL + "userid=" + getid + "&" + "token=" + gettoken + "&" + "propertyid=" + getPropertyid;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                this.onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {

                    progressDialog.dismiss();
                    Sneaker.with((Activity) context)
                            .setTitle("Deleted!")
                            .setDuration(5000)
                            .setMessage("Successfully Deleted.")
                            .sneakSuccess();

                } else if (getCode.equals("802")) {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(5000)
                            .setMessage("Re Login.")
                            .sneakError();
                    Task2 task = new Task2();
                    task.execute();

                } else if (getCode.equals("001")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Favorite not deleted")
                            .sneakError();
                    progressDialog.dismiss();
                } else if (getCode.equals("002")) {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Its your own property")
                            .sneakError();
                    progressDialog.dismiss();
                } else {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("Some Exception Occured!")
                            .sneakError();
                    progressDialog.dismiss();
                }


            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Deleting");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class Task2 extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {


            String url = LOGOUT_URL + "?userid=" + getid + "&" + "token=" + gettoken;
            //  Log.e("logout", url);


            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);
                String getCode = jsonObject.getString("ErrorCode");
                if (getCode.equals("000")) {
                    Toast.makeText(context, "Re Login!", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);


                } else if (getCode.equals("802")) {

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Toast.makeText(context, "Login Again", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);

                } else {

                    Sneaker.with((Activity) context)
                            .setTitle("Error!!")
                            .setDuration(4000)
                            .setMessage("Some Exception Occured!")
                            .sneakError();
                    progressDialog.dismiss();
                }


            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Logging Out");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

}

