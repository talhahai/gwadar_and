package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 10/17/2017.
 */

public class SearchSubSchemeListModel {
    public String name;
    public String id;

    public SearchSubSchemeListModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
