package com.example.shariqkhan.gawadar247.models;

import java.net.IDN;

/**
 * Created by ShariqKhan on 10/8/2017.
 */

public class PlotModel {

    public String ID;
    public String NAME;

    public String TYPE;
    public String AREA;
    private String HasDuplicate;

    public PlotModel()
    {

    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getAREA() {
        return AREA;
    }

    public void setAREA(String AREA) {
        this.AREA = AREA;
    }

    public String getHasDuplicate() {
        return HasDuplicate;
    }

    public void setHasDuplicate(String hasDuplicate) {
        HasDuplicate = hasDuplicate;
    }
}
