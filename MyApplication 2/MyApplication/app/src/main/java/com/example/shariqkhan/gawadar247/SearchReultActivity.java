package com.example.shariqkhan.gawadar247;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;

import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.DaniyalClasses.Data;
import com.example.shariqkhan.gawadar247.DaniyalClasses.Recycleradapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchReultActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public TextView textView;
    AlertDialog.Builder builder;
    public RecyclerView recyclerView;
    public RecyclerView.Adapter adapter;
    public RecyclerView.LayoutManager layoutManager;
    static String getLayer1name;
    public ArrayList<Data> arraylist = new ArrayList<>();
    ViewPager viewPager;
    TabLayout tabLayout;
    ImageView imageFilter;
    public static String[] Layer1name;
    public static String[] Layer2name;
    public static String[] sort= {"Low to High", "High to Low"};
    String array[];
    static String getAreaFrom;
    static String getAreaTo;
    static String getPriceFrom;
    static String getPriceTo;
    static String getSorttype;


    static SearchReultActivity ct;
    SearchReultActivityPager sectionPagerAdapter;
    public static SearchReultActivity searchReultActivity;
    static String getLayer2name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ct = this;

        setContentView(R.layout.activity_search_reult);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

        //   overridePendingTransition(R.anim.slide_in_left, 0);

        ArrayList<String> getList = getIntent().getStringArrayListExtra("Values");
        array = new String[getList.size()];

        for (int i = 0; i < getList.size(); i++) {
            array[i] = getList.get(i);
        }

        tabLayout = (TabLayout) findViewById(R.id.main_tab);
        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        textView = (TextView) toolbar.findViewById(R.id.tollbarText);
        imageFilter = (ImageView) toolbar.findViewById(R.id.filterImage);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        layoutManager = new LinearLayoutManager(this);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });

        imageFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(SearchReultActivity.this);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();


                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

                dialog.setContentView(R.layout.search_filter_dialog);
                dialog.getWindow().setLayout(lp.width, lp.height);

                final EditText Layer1dropdown = (EditText) dialog.findViewById(R.id.Layer1dropdown);
                final EditText Layer2dropdown = (EditText) dialog.findViewById(R.id.Layer2dropdown);
                final EditText areafrom = (EditText) dialog.findViewById(R.id.areaFrom);
                final EditText areaTo = (EditText) dialog.findViewById(R.id.areaTo);
                final EditText priceFrom = (EditText) dialog.findViewById(R.id.priceFrom);
                final EditText PriceTo = (EditText) dialog.findViewById(R.id.PriceTo);
                final EditText SortTo = (EditText) dialog.findViewById(R.id.sort) ;

                Button ok = dialog.findViewById(R.id.ok);
                Button cancel = dialog.findViewById(R.id.cancel);
                dialog.show();

                Layer1dropdown.setOnTouchListener(new View.OnTouchListener() {
                    AlertDialog alertDialog;

                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {

                            builder = new AlertDialog.Builder(SearchReultActivity.this);
                            builder.setTitle("Select");
                            builder.setItems(SearchReultActivity.Layer1name, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getLayer1name = Layer1name[i];
                                    Layer1dropdown.setText(getLayer1name);
                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog = builder.create();
                            alertDialog.show();

                        }


                        return true;
                    }
                });

                Layer2dropdown.setOnTouchListener(new View.OnTouchListener() {
                    AlertDialog alertDialog;

                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {

                            builder = new AlertDialog.Builder(SearchReultActivity.this);
                            builder.setTitle("Select");
                            builder.setItems(SearchReultActivity.Layer2name, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getLayer2name = Layer2name[i];
                                    Layer2dropdown.setText(getLayer2name);
                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog = builder.create();
                            alertDialog.show();

                        }


                        return true;
                    }
                });

                SortTo.setOnTouchListener(new View.OnTouchListener() {
                    AlertDialog alertDialog;
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {

                            builder = new AlertDialog.Builder(SearchReultActivity.this);
                            builder.setTitle("Sort By");
                            builder.setItems(SearchReultActivity.sort, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if(sort[i].equals("Low to High"))
                                        getSorttype = "0";
                                    else
                                        getSorttype = "1";

                                    SortTo.setText(sort[i].toString());
                                    dialogInterface.dismiss();
                                }
                            });
                            alertDialog = builder.create();
                            alertDialog.show();
                        }


                        return true;
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getAreaFrom = areafrom.getText().toString();
                        getAreaTo = areaTo.getText().toString();
                        getPriceFrom = priceFrom.getText().toString();
                        getPriceTo = PriceTo.getText().toString();

//                        Log.e("Layer1data", getLayer1name);
//                        Log.e("Layer2data", getLayer2name);
//                        Log.e("AreaFrom", getAreaFrom);
//                        Log.e("AreaTo", getAreaTo);
//                        Log.e("PriceFrom", getPriceFrom);
//                        Log.e("PriceTo", getPriceTo);

                        Intent intent = new Intent(SearchReultActivity.this, FilteredActivity.class);
                        startActivity(intent);
                        dialog.dismiss();

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }

        });
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //     getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.main_viewpager);
        sectionPagerAdapter = new SearchReultActivityPager(getSupportFragmentManager(), array);

        viewPager.setAdapter(sectionPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }


    @Override
    protected void onPause() {
        super.onPause();

        // Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);

        // startActivity(intent);
    }

    public static void giveInstance() {
        Intent intent = new Intent(ct, MainActivity.class);
        ct.finish();
        Log.e("insideInstanceOfSearch", "");
        ct.startActivity(intent);
    }


}
