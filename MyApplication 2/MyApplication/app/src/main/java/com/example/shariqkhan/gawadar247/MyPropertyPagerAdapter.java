package com.example.shariqkhan.gawadar247;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.shariqkhan.gawadar247.FavouriteSales;
import com.example.shariqkhan.gawadar247.FavouriteWanted;


public class MyPropertyPagerAdapter extends FragmentPagerAdapter {
    public MyPropertyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                PropertySales wanted = new PropertySales();
                return wanted;
            case 1:
                PropertyWanted sales = new PropertyWanted();
                return sales;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Sales";
            case 1:
                return "Wanted";
            default:
                return null;
        }

    }
}


