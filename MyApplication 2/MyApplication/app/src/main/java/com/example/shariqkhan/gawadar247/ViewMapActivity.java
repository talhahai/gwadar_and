package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.Utils.NavViewHelper;
import com.example.shariqkhan.gawadar247.adapters.onTaskCompleted;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.irozon.sneaker.Sneaker;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONException;
import org.json.JSONObject;

public class ViewMapActivity extends AppCompatActivity {

    Toolbar toolbar;
    private BottomNavigationViewEx bottomNavigationViewEx;
    TextView toolbarText;
    ImageView imageView;
    WebView webView;



    public ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.masterplanactivity);
        overridePendingTransition(0, 0);
        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        webView = (WebView) findViewById(R.id.image_webview);

        setSupportActionBar(toolbar);
        bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottom_nav_bar);
        NavViewHelper.modifyBottomNavBarEx(bottomNavigationViewEx);
        NavViewHelper.enableNavigation(ViewMapActivity.this, bottomNavigationViewEx);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(0);


        toolbarText = (TextView) toolbar.findViewById(R.id.tollbarText);
        toolbarText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        progressDialog = new ProgressDialog(ViewMapActivity.this, R.style.MyAlertDialogStyle);
        progressDialog.setTitle("Loading");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Intent intent = getIntent();
        String img = intent.getStringExtra("image");
        String text = intent.getStringExtra("mapName");

        if(text != null){
            toolbarText.setText(text);
        }


        if(img != null){

            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLoadWithOverviewMode(true);

            webView.loadUrl(img);
            webView.setWebViewClient(new MyWebViewClient());

        }

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressDialog.show();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressDialog.dismiss();
            super.onPageFinished(view, url);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
