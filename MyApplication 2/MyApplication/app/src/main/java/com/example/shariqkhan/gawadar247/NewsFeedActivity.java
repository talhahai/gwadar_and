package com.example.shariqkhan.gawadar247;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.shariqkhan.gawadar247.Utils.NavViewHelper;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.FormModel;
import com.example.shariqkhan.gawadar247.models.NewsFeedModel;
import com.example.shariqkhan.gawadar247.models.newsFlashModel;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewsFeedActivity extends AppCompatActivity {

    public static int Act_Num = 1;
    BottomNavigationViewEx bottomNavigationViewEx;
    TextView textView;
    public static int activityCheck = 0;
    Toolbar toolbar;
    public static String NEWSFEED_URL = "http://www.gwadar247.pk/api/newsflash.php?";
    SharedPreferences prefs;
    private String getuserid;
    public ProgressDialog progressDialog;
    private String gettoken;
    ArrayList<newsFlashModel> listViewItems = new ArrayList<>();

    RecyclerView recyclerView;

    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsfeed);

        toolbar = (Toolbar) findViewById(R.id.main_app_bar);

        textView = (TextView) toolbar.findViewById(R.id.tollbarText);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        setSupportActionBar(toolbar);


        prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);

        getuserid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);

        recyclerView = (RecyclerView) findViewById(R.id.property_recycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);

        adapter = new favouriteAdapter5(listViewItems, getApplicationContext());
        recyclerView.setAdapter(adapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Task task = new Task();
        task.execute();

    }


    private class Task extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {


            String url = NEWSFEED_URL + "userid=" + getuserid + "&" + "token=" + gettoken;

            //  Log.e("url", url);

            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s != null) {

                    super.onPostExecute(s);

                    JSONObject jsonObject = new JSONObject(s);

                    String getCode = jsonObject.getString("ErrorCode");
                    if (getCode.equals("000")) {


                        JSONArray jsonArray
                                = jsonObject.getJSONArray("News");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            newsFlashModel model = new newsFlashModel();
                            JSONObject js = jsonArray.getJSONObject(i);

                            model.id = js.getString("ID");
                            model.date = js.getString("Date");
                            model.image = js.getString("Image");

                            listViewItems.add(model);

                        }
                        adapter.notifyDataSetChanged();
                        progressDialog.dismiss();


                    } else {
                        progressDialog.dismiss();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                progressDialog.dismiss();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NewsFeedActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }


}
