package com.example.shariqkhan.gawadar247.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.shariqkhan.gawadar247.FavouriteSales;
import com.example.shariqkhan.gawadar247.FavouriteWanted;


public class FavouritePagerAdapter extends FragmentPagerAdapter {
    public FavouritePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FavouriteSales wanted = new FavouriteSales();
                return wanted;
            case 1:
                FavouriteWanted sales = new FavouriteWanted();
                return sales;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Sales";
            case 1:
                return "Wanted";
            default:
                return null;
        }

    }
}


