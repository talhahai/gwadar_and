package com.example.shariqkhan.gawadar247.viewholders;

/**
 * Created by ShariqKhan on 8/30/2017.
 */

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.R;
import com.example.shariqkhan.gawadar247.adapters.GenreAdapter;
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by Brad on 12/18/2016.
 */

public class GenreViewHolder extends GroupViewHolder {

    private TextView genreTitle;


    public View view;
    public ImageView genreImage;

    public GenreViewHolder(View itemView) {
        super(itemView);

        genreTitle = (TextView) itemView.findViewById(R.id.list_item_genre_name);

        genreImage = (ImageView) itemView.findViewById(R.id.list_item_genre_icon);
    }


    public void setGenreName(String name) {
        genreTitle.setText(name);
    }



    @Override
    public void expand() {
       // super.expand();

        genreImage.setImageResource(R.drawable.ic_check);
      // Toast.makeText(view.getContext(),genreTitle.getText().toString(), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void collapse() {
        //super.collapse();
        genreImage.setImageResource(R.drawable.ic_ucheck);
        GenreAdapter.floatingActionButton.setVisibility(View.GONE);
    }

}