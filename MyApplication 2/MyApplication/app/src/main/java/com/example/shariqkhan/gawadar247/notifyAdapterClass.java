package com.example.shariqkhan.gawadar247;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.adapters.onTaskCompleted;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.FavouriteModel;
import com.example.shariqkhan.gawadar247.models.NewsFeedModel;
import com.example.shariqkhan.gawadar247.models.NotificationModel;
import com.irozon.sneaker.Sneaker;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ShariqKhan on 10/12/2017.
 */


public class notifyAdapterClass extends RecyclerView.Adapter<notifyAdapterClass.Viewholder> {
    public ArrayList<NotificationModel> arrayList = new ArrayList<>();
    public Context context;
    public ArrayList<NewsFeedModel>arrayList2 = new ArrayList<>();
    public static String URL = "http://www.gwadar247.pk/api/respondrequest.php?";
    public SharedPreferences prefs;

    public String getid;
    public String gettoken;
    public String requestId;

    public ProgressDialog progressDialog;

    notifyAdapterClass(ArrayList<NotificationModel> arrayList,ArrayList<NewsFeedModel>arrayList2, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.arrayList2 = arrayList2;

    }

    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_notification_layout, null, false);
        return new notifyAdapterClass.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(Viewholder holder, final int position) {
        prefs = context.getSharedPreferences("SharedPreferences", MODE_PRIVATE);

        getid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);

        NotificationModel model = arrayList.get(position);
        NewsFeedModel model2 = arrayList2.get(position);

        final String line1text = model2.getLine1Text();
        final String line2text = model2.getLine2Text();
        final String line3text = model2.getLine3Text();
        final String line4text = model2.getLine4Text();
        final String line5text = model2.getLine5Text();
        final String line6text = model2.getLine6Text();

        final String line1icon = model2.getLine1Icon().split("\\.")[0];
        final String line2icon = model2.getLine2Icon().split("\\.")[0];
        final String line3icon = model2.getLine3Icon().split("\\.")[0];
        final String line4icon = model2.getLine4Icon().split("\\.")[0];
        final String line5icon = model2.getLine5Icon().split("\\.")[0];
        final String line6icon = model2.getLine6Icon().split("\\.")[0];


        final String price = model2.getPrice();
        final String Image = model2.getImage();

        final String Type = model.Type;
        final String Message = model.Message;
        String dateTime = model.NotifyTime;
        final String notifyType = model.NotificationType;
        holder.notificationTextView.setText(Message);
        requestId = model.getRequestID();


        if (position % 2 == 0) holder.rel.setBackgroundColor(Color.rgb(242, 242, 242));

        holder.date.setText(dateTime);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NotificationModel model = arrayList.get(position);
                NewsFeedModel model2 = arrayList2.get(position);
                requestId = model.getRequestID();

                if (Type.equals("0")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.for_notification_layout);
                    TextView amount = dialog.findViewById(R.id.amount);
                    ImageView image = (ImageView)dialog.findViewById(R.id.profile_image);

                    TextView line1  = (TextView) dialog.findViewById(R.id.line1text);
                    TextView line2  = (TextView) dialog.findViewById(R.id.line2text);
                    TextView line3  = (TextView) dialog.findViewById(R.id.line3text);
                    TextView line4  = (TextView) dialog.findViewById(R.id.line4text);
                    TextView line5  = (TextView) dialog.findViewById(R.id.line5text);
                    TextView line6  = (TextView) dialog.findViewById(R.id.line6text);

                    amount.setText(price);
                    if(!line1icon.equals("empty.png") && !line1text.equals("")){
                        line1.setText(" "+line1text);
                        line1.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line1icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line1.setVisibility(View.GONE);

                    if(!line2icon.equals("empty.png") && !line2text.equals("")){
                        line2.setText(" "+line2text);
                        line2.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line2icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line2.setVisibility(View.GONE);

                    if(!line3icon.equals("empty.png") && !line3text.equals("")){
                        line3.setText(" "+line3text);
                        line3.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line3icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line3.setVisibility(View.GONE);

                    if(!line4icon.equals("empty.png") && !line4text.equals("")){
                        line4.setText(" "+line4text);
                        line4.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line4icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line4.setVisibility(View.GONE);

                    if(!line5icon.equals("empty.png") && !line5text.equals("")){
                        line5.setText(" "+line5text);
                        line5.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line5icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line5.setVisibility(View.GONE);

                    if(!line6icon.equals("empty.png") && !line6text.equals("")){
                        line6.setText(" "+line6text);
                        line6.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line6icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line6.setVisibility(View.GONE);

                    Picasso.with(context).load(Image).into(image);


                    dialog.getWindow().setLayout(lp.width, lp.height);
                    Button ok = dialog.findViewById(R.id.add_to_cart);
                    ok.setText("Accept");

                    Button cancel = dialog.findViewById(R.id.sendenquiry);
                    cancel.setText("Reject");
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            respondRequest respondRequest = new respondRequest(true, new onTaskCompleted() {
                                @Override
                                public void onTaskCompleted(String code) {
                                    if(code.equals("000")){
                                        Sneaker.with((Activity) context)
                                                .setTitle("Accepted!")
                                                .setDuration(5000)
                                                .setMessage("Offer Accepted!")
                                                .sneakSuccess();
                                        dialog.dismiss();
                                        notifyDataSetChanged();
                                    }
                                }
                            });
                            respondRequest.execute();

                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            respondRequest respondRequest = new respondRequest(false, new onTaskCompleted() {
                                @Override
                                public void onTaskCompleted(String code) {
                                    if(code.equals("000")){
                                        Sneaker.with((Activity) context)
                                                .setTitle("Rejected!")
                                                .setDuration(5000)
                                                .setMessage("Offer Rejected!")
                                                .sneakError();
                                        dialog.dismiss();
                                        notifyDataSetChanged();

                                    }

                                }
                            });
                            respondRequest.execute();


                        }
                    });
                    dialog.show();
                } else if (Type.equals("1")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.offer_pending);
                    dialog.getWindow().setLayout(lp.width, lp.height);
                    TextView amount = dialog.findViewById(R.id.amount);

                    TextView line1  = (TextView) dialog.findViewById(R.id.line1text);
                    TextView line2  = (TextView) dialog.findViewById(R.id.line2text);
                    TextView line3  = (TextView) dialog.findViewById(R.id.line3text);
                    TextView line4  = (TextView) dialog.findViewById(R.id.line4text);
                    TextView line5  = (TextView) dialog.findViewById(R.id.line5text);
                    TextView line6  = (TextView) dialog.findViewById(R.id.line6text);

                    amount.setText(price);

                    if(!line1icon.equals("empty.png") && !line1text.equals("")){
                        line1.setText(" "+line1text);
                        line1.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line1icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line1.setVisibility(View.GONE);

                    if(!line2icon.equals("empty.png") && !line2text.equals("")){
                        line2.setText(" "+line2text);
                        line2.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line2icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line2.setVisibility(View.GONE);

                    if(!line3icon.equals("empty.png") && !line3text.equals("")){
                        line3.setText(" "+line3text);
                        line3.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line3icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line3.setVisibility(View.GONE);

                    if(!line4icon.equals("empty.png") && !line4text.equals("")){
                        line4.setText(" "+line4text);
                        line4.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line4icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line4.setVisibility(View.GONE);

                    if(!line5icon.equals("empty.png") && !line5text.equals("")){
                        line5.setText(" "+line5text);
                        line5.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line5icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line5.setVisibility(View.GONE);

                    if(!line6icon.equals("empty.png") && !line6text.equals("")){
                        line6.setText(" "+line6text);
                        line6.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line6icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line6.setVisibility(View.GONE);

                    dialog.show();
                } else if (Type.equals("2")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.offer_already_accepted);
                    dialog.getWindow().setLayout(lp.width, lp.height);
                    TextView amount = dialog.findViewById(R.id.amount);

                    TextView line1  = (TextView) dialog.findViewById(R.id.line1text);
                    TextView line2  = (TextView) dialog.findViewById(R.id.line2text);
                    TextView line3  = (TextView) dialog.findViewById(R.id.line3text);
                    TextView line4  = (TextView) dialog.findViewById(R.id.line4text);
                    TextView line5  = (TextView) dialog.findViewById(R.id.line5text);
                    TextView line6  = (TextView) dialog.findViewById(R.id.line6text);

                    amount.setText(price);
                    if(!line1icon.equals("empty.png") && !line1text.equals("")){
                        line1.setText(" "+line1text);
                        line1.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line1icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line1.setVisibility(View.GONE);

                    if(!line2icon.equals("empty.png") && !line2text.equals("")){
                        line2.setText(" "+line2text);
                        line2.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line2icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line2.setVisibility(View.GONE);

                    if(!line3icon.equals("empty.png") && !line3text.equals("")){
                        line3.setText(" "+line3text);
                        line3.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line3icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line3.setVisibility(View.GONE);

                    if(!line4icon.equals("empty.png") && !line4text.equals("")){
                        line4.setText(" "+line4text);
                        line4.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line4icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line4.setVisibility(View.GONE);

                    if(!line5icon.equals("empty.png") && !line5text.equals("")){
                        line5.setText(" "+line5text);
                        line5.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line5icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line5.setVisibility(View.GONE);

                    if(!line6icon.equals("empty.png") && !line6text.equals("")){
                        line6.setText(" "+line6text);
                        line6.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line6icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line6.setVisibility(View.GONE);

                    dialog.show();
                } else if (Type.equals("3")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.offer_accepted);
                    dialog.getWindow().setLayout(lp.width, lp.height);
                    TextView amount = dialog.findViewById(R.id.amount);

                    TextView line1  = (TextView) dialog.findViewById(R.id.line1text);
                    TextView line2  = (TextView) dialog.findViewById(R.id.line2text);
                    TextView line3  = (TextView) dialog.findViewById(R.id.line3text);
                    TextView line4  = (TextView) dialog.findViewById(R.id.line4text);
                    TextView line5  = (TextView) dialog.findViewById(R.id.line5text);
                    TextView line6  = (TextView) dialog.findViewById(R.id.line6text);

                    amount.setText(price);
                    if(!line1icon.equals("empty.png") && !line1text.equals("")){
                        line1.setText(" "+line1text);
                        line1.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line1icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line1.setVisibility(View.GONE);

                    if(!line2icon.equals("empty.png") && !line2text.equals("")){
                        line2.setText(" "+line2text);
                        line2.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line2icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line2.setVisibility(View.GONE);

                    if(!line3icon.equals("empty.png") && !line3text.equals("")){
                        line3.setText(" "+line3text);
                        line3.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line3icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line3.setVisibility(View.GONE);

                    if(!line4icon.equals("empty.png") && !line4text.equals("")){
                        line4.setText(" "+line4text);
                        line4.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line4icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line4.setVisibility(View.GONE);

                    if(!line5icon.equals("empty.png") && !line5text.equals("")){
                        line5.setText(" "+line5text);
                        line5.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line5icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line5.setVisibility(View.GONE);

                    if(!line6icon.equals("empty.png") && !line6text.equals("")){
                        line6.setText(" "+line6text);
                        line6.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line6icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line6.setVisibility(View.GONE);
                    dialog.show();

                } else if (Type.equals("4")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.offer_accepted);
                    dialog.getWindow().setLayout(lp.width, lp.height);
                    TextView amount = dialog.findViewById(R.id.amount);

                    TextView line1  = (TextView) dialog.findViewById(R.id.line1text);
                    TextView line2  = (TextView) dialog.findViewById(R.id.line2text);
                    TextView line3  = (TextView) dialog.findViewById(R.id.line3text);
                    TextView line4  = (TextView) dialog.findViewById(R.id.line4text);
                    TextView line5  = (TextView) dialog.findViewById(R.id.line5text);
                    TextView line6  = (TextView) dialog.findViewById(R.id.line6text);

                    amount.setText(price);
                    if(!line1icon.equals("empty.png") && !line1text.equals("")){
                        line1.setText(" "+line1text);
                        line1.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line1icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line1.setVisibility(View.GONE);

                    if(!line2icon.equals("empty.png") && !line2text.equals("")){
                        line2.setText(" "+line2text);
                        line2.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line2icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line2.setVisibility(View.GONE);

                    if(!line3icon.equals("empty.png") && !line3text.equals("")){
                        line3.setText(" "+line3text);
                        line3.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line3icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line3.setVisibility(View.GONE);

                    if(!line4icon.equals("empty.png") && !line4text.equals("")){
                        line4.setText(" "+line4text);
                        line4.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line4icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line4.setVisibility(View.GONE);

                    if(!line5icon.equals("empty.png") && !line5text.equals("")){
                        line5.setText(" "+line5text);
                        line5.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line5icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line5.setVisibility(View.GONE);

                    if(!line6icon.equals("empty.png") && !line6text.equals("")){
                        line6.setText(" "+line6text);
                        line6.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line6icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line6.setVisibility(View.GONE);
                    dialog.show();
                } else if (Type.equals("5")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.offer_already_rejected);
                    dialog.getWindow().setLayout(lp.width, lp.height);

                    TextView amount = dialog.findViewById(R.id.amount);

                    TextView line1  = (TextView) dialog.findViewById(R.id.line1text);
                    TextView line2  = (TextView) dialog.findViewById(R.id.line2text);
                    TextView line3  = (TextView) dialog.findViewById(R.id.line3text);
                    TextView line4  = (TextView) dialog.findViewById(R.id.line4text);
                    TextView line5  = (TextView) dialog.findViewById(R.id.line5text);
                    TextView line6  = (TextView) dialog.findViewById(R.id.line6text);

                    amount.setText(price);
                    if(!line1icon.equals("empty.png") && !line1text.equals("")){
                        line1.setText(" "+line1text);
                        line1.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line1icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line1.setVisibility(View.GONE);

                    if(!line2icon.equals("empty.png") && !line2text.equals("")){
                        line2.setText(" "+line2text);
                        line2.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line2icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line2.setVisibility(View.GONE);

                    if(!line3icon.equals("empty.png") && !line3text.equals("")){
                        line3.setText(" "+line3text);
                        line3.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line3icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line3.setVisibility(View.GONE);

                    if(!line4icon.equals("empty.png") && !line4text.equals("")){
                        line4.setText(" "+line4text);
                        line4.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line4icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line4.setVisibility(View.GONE);

//                    if(!line5icon.equals("empty.png") && !line5text.equals("") && line5 != null){
//                        line5.setText(" "+line5text);
//                        line5.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line5icon,"drawable", context.getPackageName()), 0, 0, 0);
//                    }
//                    else
//                        line5.setVisibility(View.GONE);

                    if(!line6icon.equals("empty.png") && !line6text.equals("")){
                        line6.setText(" "+line6text);
                        line6.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line6icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line6.setVisibility(View.GONE);
                    dialog.show();
                } else if (Type.equals("6")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.offer_rejected);
                    dialog.getWindow().setLayout(lp.width, lp.height);

                    dialog.show();

                } else if (Type.equals("7")) {
                    final Dialog dialog = new Dialog(context);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    dialog.setContentView(R.layout.user_rejected);
                    dialog.getWindow().setLayout(lp.width, lp.height);

                    TextView amount = dialog.findViewById(R.id.amount);

                    TextView line1  = (TextView) dialog.findViewById(R.id.line1text);
                    TextView line2  = (TextView) dialog.findViewById(R.id.line2text);
                    TextView line3  = (TextView) dialog.findViewById(R.id.line3text);
                    TextView line4  = (TextView) dialog.findViewById(R.id.line4text);
                    TextView line5  = (TextView) dialog.findViewById(R.id.line5text);
                    TextView line6  = (TextView) dialog.findViewById(R.id.line6text);

                    amount.setText(price);
                    if(!line1icon.equals("empty.png") && !line1text.equals("")){
                        line1.setText(" "+line1text);
                        line1.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line1icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line1.setVisibility(View.GONE);

                    if(!line2icon.equals("empty.png") && !line2text.equals("")){
                        line2.setText(" "+line2text);
                        line2.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line2icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line2.setVisibility(View.GONE);

                    if(!line3icon.equals("empty.png") && !line3text.equals("")){
                        line3.setText(" "+line3text);
                        line3.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line3icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line3.setVisibility(View.GONE);

                    if(!line4icon.equals("empty.png") && !line4text.equals("")){
                        line4.setText(" "+line4text);
                        line4.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line4icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line4.setVisibility(View.GONE);

                    if(!line5icon.equals("empty.png") && !line5text.equals("")){
                        line5.setText(" "+line5text);
                        line5.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line5icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line5.setVisibility(View.GONE);

                    if(!line6icon.equals("empty.png") && !line6text.equals("")){
                        line6.setText(" "+line6text);
                        line6.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getIdentifier(line6icon,"drawable", context.getPackageName()), 0, 0, 0);
                    }
                    else
                        line6.setVisibility(View.GONE);
                    dialog.show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        TextView notificationTextView;
        RelativeLayout rel;
        TextView date;
        View view;

        public Viewholder(View itemView) {
            super(itemView);
            view = itemView;
            notificationTextView = (TextView) itemView.findViewById(R.id.notifyTextView);
            rel = (RelativeLayout) itemView.findViewById(R.id.shade_or_not);
            date = (TextView) itemView.findViewById(R.id.date);

        }
    }

    public class respondRequest extends AsyncTask<Object, Object, String> {

        boolean flag;
        onTaskCompleted onTaskCompleted;


        public respondRequest(boolean flag, onTaskCompleted onTaskCompleted){
            this.onTaskCompleted = onTaskCompleted;
            this.flag = flag;
        }

        @Override
        protected String doInBackground(Object... voids) {

            String favURL = URL + "userid=" + getid + "&" + "token=" + gettoken + "&" + "requestid=" + requestId + "&" + "response=" + this.flag
                    ;

            Log.e("url", favURL);

            String response = getHttpData.getData(favURL);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                onTaskCompleted.onTaskCompleted(getCode);
                if (getCode.equals("000")) {
                    //    Recyclerholder.addtocart.setText("Favourite");

//                    Log.e("EnquiryIssue", "Enquiry Deleted");
                    progressDialog.dismiss();
//                    Sneaker.with((Activity) context)
//                            .setTitle("Successfull")
//                            .setDuration(4000)
//                            .setMessage("Request Sent")
//                            .sneakSuccess();
//                    notifyDataSetChanged();


                } else if (getCode.equals("802")) {

                    Toast.makeText(context, "Login Session Expired!", Toast.LENGTH_SHORT).show();
//                    favouriteAdapter4.Task2 task = new favouriteAdapter4.Task2();
//                    task.execute();

                } else {
                    Sneaker.with((Activity) context)
                            .setTitle("Error!")
                            .setDuration(4000)
                            .setMessage("Failed to Respond Request")
                            .sneakError();
                    progressDialog.dismiss();

                    notifyDataSetChanged();


                }

            } catch (JSONException e) {
                Sneaker.with((Activity) context)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
            }

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Requesting");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }
}
