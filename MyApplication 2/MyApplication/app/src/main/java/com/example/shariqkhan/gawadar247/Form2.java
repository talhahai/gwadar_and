package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.irozon.sneaker.Sneaker;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Form2 extends AppCompatActivity {
    EditText SearFron;
    String SeaFrontArray[] = {"0", "1", "2", "3"};
    String RoadFrontArray[] = {"0", "1", "2"};
    String ssFront = "";
    ProgressDialog progressDialog;
    EditText RoadFront;

    EditText Road1name;
    EditText Road2name;
    EditText Road1acre;
    EditText Road2acre;


    String sendPriceofAcre = "";
    String totalPrice = "";


    String sendRoad1name = "";
    String sendRoad2name = "";
    String sendRoad1acre = "";
    String sendRoad2acre = "";


    String propertyType = "Sale";
    String url = "";


    FrameLayout wantedFrameLayout;

    String subSchemeId;
    String subShemeName;
    String schemeid;
    LinearLayout salesFrameLayout;
    Button submitOfHidden;
    Button Submit;

    String commisionPrice = "";
    EditText priceofAcre;
    EditText totalPriceEdt;

    private String rrFront = "";
    TextView demoTotalPrice;
    String TotalPrice = "";
    SharedPreferences prefs;
    String gettoken;
    String getuserid;
    private String[] RoadsArray;
    TextView demopriceCom;


    Button formSalesButton;
    Button formWantedButton;
    Toolbar toolbar;
    LinearLayout areaContainer;
    EditText areatext;

    private void resetFieldsWhenWanted() {
        propertyType = "Wanted";
        demoTotalPrice.setText("");
        demopriceCom.setText("");
        priceofAcre.setText("");
        //totalPriceEdt.setText("");
        SearFron.setText("");
        RoadFront.setText("");
        ssFront = "";
        rrFront = "";
        Road1name.setText("");
        Road1acre.setText("");
        Road2name.setText("");
        Road2acre.setText("");
        reset();

    }
    private void resetFieldsWhenSale() {
        propertyType = "Sale";
        demoTotalPrice.setText("");
        demopriceCom.setText("");
        priceofAcre.setText("");
        areatext.setText("");

        SearFron.setText("");
        RoadFront.setText("");
        ssFront = "";
        rrFront = "";
        Road1name.setText("");
        Road1acre.setText("");
        Road2name.setText("");
        Road2acre.setText("");
        reset();
    }

    public void reset(){
//        schemeid = "";
//
//        subSchemeId = "";

        ssFront = "";
        rrFront = "";
        sendRoad1name = "";
        sendRoad2name = "";
        sendRoad1acre = "";
        sendRoad2acre = "";


        if (propertyType.equals("Sale")) {
            sendPriceofAcre = "";
            TotalPrice = "";
        }

        LinearLayout Road1NameLayout = (LinearLayout) findViewById(R.id.Road1NameLayout);
        Road1NameLayout.setVisibility(LinearLayout.GONE);
        LinearLayout Road1AcreLayout = (LinearLayout) findViewById(R.id.Road1AcreLayout);
        Road1AcreLayout.setVisibility(LinearLayout.GONE);

        LinearLayout Road2NameLayout = (LinearLayout) findViewById(R.id.Road2NameLayout);
        Road2NameLayout.setVisibility(LinearLayout.GONE);
        LinearLayout Road2AcreLayout = (LinearLayout) findViewById(R.id.Road2AcreLayout);
        Road2AcreLayout.setVisibility(LinearLayout.GONE);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form2);

        demopriceCom = (TextView) findViewById(R.id.demoPriceComission);
        demoTotalPrice = (TextView) findViewById(R.id.demoTotalPrice);
        SearFron = (EditText) findViewById(R.id.SchemeEditText);
        RoadFront = (EditText) findViewById(R.id.RoadFrontEditText);
        Submit = (Button) findViewById(R.id.submitbutton);
        submitOfHidden = (Button) findViewById(R.id.submitbuttonofhidden);
        areaContainer= (LinearLayout)findViewById(R.id.linearAddress);
        areatext = (EditText)findViewById(R.id.areaEditText);

        prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        getuserid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);


        Road1name = (EditText) findViewById(R.id.StreetEditText);
        Road2name = (EditText) findViewById(R.id.roadtwoedttext);

        subSchemeId = getIntent().getStringExtra("subchemeid");
        subShemeName = getIntent().getStringExtra("schemename");
        schemeid = getIntent().getStringExtra("schemeid");

        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        Log.e("subschemeid", subSchemeId);
        Log.e("schemename", subShemeName);
        Log.e("schemeid", schemeid);

        Task task = new Task();
        task.execute();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        FloatingActionButton floatingActionButtonForm2 = (FloatingActionButton) findViewById(R.id.floatingActionButtonMap2);
        floatingActionButtonForm2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Form2.this, SchemeMapAddProperty.class);
                intent.putExtra("subschemeid", subSchemeId+"");
                startActivity(intent);
            }
        });

        Road2name.setOnTouchListener(new View.OnTouchListener() {
            AlertDialog dialog;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Form2.this);
                    builder.setTitle("Select");
                    builder.setItems(RoadsArray, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            sendRoad2name = RoadsArray[i];
                            Road2name.setText(sendRoad2name);
                            dialogInterface.dismiss();
                        }
                    });

                    dialog = builder.create();
                    dialog.show();
                }
                return true;

            }

        });

        Road1name.setOnTouchListener(new View.OnTouchListener() {
            AlertDialog dialog;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Form2.this);
                    builder.setTitle("Select");
                    builder.setItems(RoadsArray, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            sendRoad1name = RoadsArray[i];
                            Road1name.setText(sendRoad1name);
                            dialogInterface.dismiss();
                        }
                    });

                    dialog = builder.create();
                    dialog.show();
                }
                return true;

            }

        });


        priceofAcre = (EditText) findViewById(R.id.pp);


        totalPriceEdt = (EditText) findViewById(R.id.priceofframeEditText2);

        totalPriceEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               if (getCurrentFocus() == totalPriceEdt)
                if (charSequence != null) {
                    totalPrice = charSequence.toString();


                    double com = Double.valueOf(totalPrice);
                    double ans = com * 0.01;
                    commisionPrice = String.valueOf(ans);
                    double dd = com + ans;
                    String con = String.valueOf(dd);
                    TotalPrice = (String) con;
                    demoTotalPrice.setText(TotalPrice);

                    demopriceCom.setText(commisionPrice);

                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Road1acre = (EditText) findViewById(R.id.plotEditText);
        Road2acre = (EditText) findViewById(R.id.roadtwoacreedttext);


        wantedFrameLayout = (FrameLayout) findViewById(R.id.hiddenFrameLayout);
        salesFrameLayout = (LinearLayout) findViewById(R.id.frameLayoutOfPrice);

        SearFron.setOnTouchListener(new View.OnTouchListener() {
            AlertDialog dialog;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Form2.this);
                    builder.setTitle("Select");
                    builder.setItems(SeaFrontArray, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            SearFron.setText(SeaFrontArray[i]);
                            ssFront = SeaFrontArray[i];
                            dialogInterface.dismiss();

                        }
                    });

                    dialog = builder.create();
                    dialog.show();

                }

                return true;

            }
        });
        RoadFront.setOnTouchListener(new View.OnTouchListener() {
            AlertDialog dialog;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Form2.this);
                    builder.setTitle("Select 12312");
                    builder.setItems(RoadFrontArray, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            RoadFront.setText(RoadFrontArray[i]);
                            rrFront = RoadFrontArray[i];
                            if (rrFront == "0")
                            {
                                LinearLayout Road1NameLayout = (LinearLayout) findViewById(R.id.Road1NameLayout);
                                Road1NameLayout.setVisibility(LinearLayout.GONE);
                                LinearLayout Road1AcreLayout = (LinearLayout) findViewById(R.id.Road1AcreLayout);
                                Road1AcreLayout.setVisibility(LinearLayout.GONE);

                                LinearLayout Road2NameLayout = (LinearLayout) findViewById(R.id.Road2NameLayout);
                                Road2NameLayout.setVisibility(LinearLayout.GONE);
                                LinearLayout Road2AcreLayout = (LinearLayout) findViewById(R.id.Road2AcreLayout);
                                Road2AcreLayout.setVisibility(LinearLayout.GONE);
                            }
                            else if (rrFront == "1")
                            {
                                LinearLayout Road1NameLayout = (LinearLayout) findViewById(R.id.Road1NameLayout);
                                Road1NameLayout.setVisibility(LinearLayout.VISIBLE);
                                LinearLayout Road1AcreLayout = (LinearLayout) findViewById(R.id.Road1AcreLayout);
                                Road1AcreLayout.setVisibility(LinearLayout.VISIBLE);

                                LinearLayout Road2NameLayout = (LinearLayout) findViewById(R.id.Road2NameLayout);
                                Road2NameLayout.setVisibility(LinearLayout.GONE);
                                LinearLayout Road2AcreLayout = (LinearLayout) findViewById(R.id.Road2AcreLayout);
                                Road2AcreLayout.setVisibility(LinearLayout.GONE);
                            }
                            else
                            {
                                LinearLayout Road1NameLayout = (LinearLayout) findViewById(R.id.Road1NameLayout);
                                Road1NameLayout.setVisibility(LinearLayout.VISIBLE);
                                LinearLayout Road1AcreLayout = (LinearLayout) findViewById(R.id.Road1AcreLayout);
                                Road1AcreLayout.setVisibility(LinearLayout.VISIBLE);

                                LinearLayout Road2NameLayout = (LinearLayout) findViewById(R.id.Road2NameLayout);
                                Road2NameLayout.setVisibility(LinearLayout.VISIBLE);
                                LinearLayout Road2AcreLayout = (LinearLayout) findViewById(R.id.Road2AcreLayout);
                                Road2AcreLayout.setVisibility(LinearLayout.VISIBLE);
                            }
                            dialogInterface.dismiss();

                        }
                    });

                    dialog = builder.create();
                    dialog.show();

                }

                return true;

            }
        });

        formSalesButton = (Button) findViewById(R.id.formSalesButton);
        formWantedButton = (Button) findViewById(R.id.formWantedButton);

        formSalesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFieldsWhenSale();
                propertyType = "Sale";
                wantedFrameLayout.setVisibility(View.GONE);
                salesFrameLayout.setVisibility(View.VISIBLE);
                areaContainer.setVisibility(View.VISIBLE);

                formSalesButton.setBackground(ContextCompat.getDrawable(Form2.this, R.drawable.pressed));
                formSalesButton.setTextColor(Color.WHITE);
                formWantedButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                formWantedButton.setBackground(ContextCompat.getDrawable(Form2.this, R.drawable.not_pressed_candr));


            }
        });


        formWantedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetFieldsWhenWanted();
                propertyType = "Wanted";
                wantedFrameLayout.setVisibility(View.VISIBLE);
                salesFrameLayout.setVisibility(View.GONE);
                areaContainer.setVisibility(View.GONE);

                formWantedButton.setBackground(ContextCompat.getDrawable(Form2.this, R.drawable.pressed));
                formWantedButton.setTextColor(Color.WHITE);
                formSalesButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                formSalesButton.setBackground(ContextCompat.getDrawable(Form2.this, R.drawable.not_pressed_candr));


            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRoad1acre = Road1acre.getText().toString();
                sendRoad2acre = Road2acre.getText().toString();
                sendPriceofAcre = priceofAcre.getText().toString();
                //totalPriceEdt.setText("");
                AddPropertyAsyncTask asyncTask = new AddPropertyAsyncTask();
                asyncTask.execute();
            }
        });

        submitOfHidden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRoad1acre = Road1acre.getText().toString();
                sendRoad2acre = Road2acre.getText().toString();

                AddPropertyAsyncTask asyncTask = new AddPropertyAsyncTask();
                asyncTask.execute();
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


    private class AddPropertyAsyncTask extends AsyncTask<String, Void, String> {

        String stream = null;
        String url;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Form2.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Adding Plot");

            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String[] params) {
            url = "http://www.gwadar247.pk/api/addpropopenland.php?userid=" + getuserid + "&token=" + gettoken;
            ;
            Log.e("Addpropertyurl", url);

            stream = getJson(url);

            return stream;

        }

        private String getJson(String url) {
            HttpClient httpClient = new DefaultHttpClient();


            HttpPost post = new HttpPost(url);

            List<NameValuePair> parameters = new ArrayList<>();
            Log.e("whichType", propertyType);
            Log.e("schemeid", schemeid);
            Log.e("roadfront", rrFront);
            Log.e("seafront", ssFront);
            Log.e("road1name", sendRoad1name);
            Log.e("road2name", sendRoad2name);
            Log.e("road1acre", sendRoad1acre);
            Log.e("road2acre", sendRoad2acre);
            if (propertyType.equals("Sale")) {
                Log.e("pricearea", sendPriceofAcre);
                Log.e("price", TotalPrice);
            }


            parameters.add(new BasicNameValuePair("propertytype", propertyType));
            parameters.add(new BasicNameValuePair("schemeid", schemeid));

            parameters.add(new BasicNameValuePair("subschemeid", subSchemeId));

            parameters.add(new BasicNameValuePair("seafront", ssFront));
            parameters.add(new BasicNameValuePair("roadfront", rrFront));
            parameters.add(new BasicNameValuePair("road1name", sendRoad1name));
            parameters.add(new BasicNameValuePair("road2name", sendRoad2name));
            parameters.add(new BasicNameValuePair("road1acre", sendRoad1acre));
            parameters.add(new BasicNameValuePair("road2acre", sendRoad2acre));


            if (propertyType.equals("Sale")) {
                parameters.add(new BasicNameValuePair("priceacre", sendPriceofAcre));
                parameters.add(new BasicNameValuePair("price", TotalPrice));
                parameters.add(new BasicNameValuePair("area", areatext.getText().toString()));

            }


            StringBuilder buffer = new StringBuilder();

            try {
                // Log.e("Insidegetjson", "insidetry");
                UrlEncodedFormEntity encoded = new UrlEncodedFormEntity(parameters);
                post.setEntity(encoded);
                HttpResponse response = httpClient.execute(post);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String Line = "";

                while ((Line = reader.readLine()) != null) {
                    Log.e("buffer", Line);
                    buffer.append(Line);


                }
                reader.close();

            } catch (Exception o) {
                Log.e("ErrorInSending", o.getMessage());

            }
            return buffer.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String errorCode = "";

            JSONObject jsonobj;
            if (s != null) {
                try {
                    jsonobj = new JSONObject(s);

                    errorCode = jsonobj.getString("ErrorCode");

                    if (errorCode.equals("000")) {
                        Sneaker.with(Form2.this)
                                .setDuration(5000)
                                .setMessage("Plot Added!")
                                .sneakSuccess();
                        if (propertyType.equals("Sale")){
                            resetFieldsWhenSale();
                            totalPriceEdt.clearFocus();
                            totalPriceEdt.setText("");

                        }else{
                            resetFieldsWhenWanted();
                        }


                    } else {
                        Sneaker.with(Form2.this)
                                .setTitle("Error!")
                                .setDuration(5000)
                                .setMessage("Plot Not Added!")
                                .sneakError();
                        Log.e("InsideAddPropertyAsync", errorCode);
                        if (propertyType.equals("Sale")){
                            resetFieldsWhenSale();

                        }else{
                            resetFieldsWhenWanted();
                        }

                    }
                } catch (JSONException e) {
                    Sneaker.with(Form2.this)
                            .setTitle("No internet connection!")
                            .setDuration(5000)
                            .setMessage("No internet connection. Retry with stable connection")
                            .sneakError();
                    Log.e("InsideAddPropertyAsync", errorCode);
                    if (propertyType.equals("Sale")){
                        resetFieldsWhenSale();
                    }else{
                        resetFieldsWhenWanted();
                    }

                }
            }
            progressDialog.dismiss();


        }
    }

    private class Task extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {


            String url = "http://www.gwadar247.pk/api/roads.php?" + "userid=" + getuserid + "&" + "token=" + gettoken + "&" + "subschemeid=" + subSchemeId;

            //  Log.e("url", url);

            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s){
            try {
            if (s == null) {
                throw new JSONException("No Internet Connection Exception");
            }

            super.onPostExecute(s);

            JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");


                if (getCode.equals("000")) {

                    JSONArray jsonArray
                            = jsonObject.getJSONArray("Roads");
                    RoadsArray = new String[jsonArray.length()];

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject JS = jsonArray.getJSONObject(i);
                        RoadsArray[i] = JS.getString("Name");

                    }

                    progressDialog.dismiss();

                } else {
                    progressDialog.dismiss();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                progressDialog.dismiss();
                Log.e("MessageOfRoads", e.getMessage());

            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Form2.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Please Wait");

            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }


    }


}
