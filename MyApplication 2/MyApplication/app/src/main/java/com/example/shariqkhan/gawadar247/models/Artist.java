package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 8/30/2017.
 */

public class Artist {
    private String name;
    private String id;

    public Artist(String id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
