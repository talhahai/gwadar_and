package com.example.shariqkhan.gawadar247;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.shariqkhan.gawadar247.adapters.favouriteAdapter;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.FavouriteModel;
import com.example.shariqkhan.gawadar247.models.LoginModel;
import com.example.shariqkhan.gawadar247.models.NewsFeedModel;
import com.example.shariqkhan.gawadar247.models.SchemeListModel;
import com.irozon.sneaker.Sneaker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ShariqKhan on 9/27/2017.
 */

public class FavouriteSales extends Fragment {

    RecyclerView recyclerView;
    public SharedPreferences prefs;
    private ProgressDialog progressDialog;
    String getid;
    String gettoken;
    ArrayList<NewsFeedModel> listViewItems = new ArrayList<>();
    public String LOGOUT_URL = "http://www.gwadar247.pk/api/logout.php";

    favouriteAdapter2 adapter;
    RecyclerView.LayoutManager layoutManager;
    public static String FAVOURITE = "http://www.gwadar247.pk/api/favoriteproperties2.php?";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.favourite_sale, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.fav_sales);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        prefs = getContext().getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        getid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);

        recyclerView.setHasFixedSize(true);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new favouriteAdapter2(listViewItems,getContext());

        recyclerView.setAdapter(adapter);

        Task task = new Task();
        task.execute();
        return view;
    }


    private class Task extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {

            String url = FAVOURITE + "userid=" + getid + "&" + "token=" + gettoken;

            Log.e("url", url);

            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                if (getCode.equals("000")) {

                    JSONObject propertiesObject
                            = jsonObject.getJSONObject("Properties");



                    JSONArray jsonArray = propertiesObject.getJSONArray("Sale");



                    for (int i = 0; i < jsonArray.length(); i++) {

                        Log.e("loop of favourites", String.valueOf(i));

                        NewsFeedModel schemelistmodel = new NewsFeedModel();
                        JSONObject js = jsonArray.getJSONObject(i);

                        //    Log.e("jsonArray", String.valueOf(jsonArray.getJSONObject(i)));


                        schemelistmodel.setID(js.getString("ID"));
                        schemelistmodel.setRating(js.getString("Rating"));
                        schemelistmodel.setIsSold(js.getString("IsSold"));
                        schemelistmodel.setIsOpenLand(js.getString("IsOpenLand"));
                        schemelistmodel.setPrice(js.getString("Price"));
                        schemelistmodel.setImage(js.getString("Image"));
                        schemelistmodel.setIsRequested(js.getString("IsRequested"));
                        schemelistmodel.setIsFavorite(js.getString("IsFavorite"));
                        schemelistmodel.setLine1Icon(js.getString("Line1Icon"));
                        schemelistmodel.setLine1Text(js.getString("Line1Text"));
                        schemelistmodel.setLine2Icon(js.getString("Line2Icon"));
                        schemelistmodel.setLine2Text(js.getString("Line2Text"));
                        schemelistmodel.setLine3Icon(js.getString("Line3Icon"));
                        schemelistmodel.setLine3Text(js.getString("Line3Text"));
                        schemelistmodel.setLine4Icon(js.getString("Line4Icon"));
                        schemelistmodel.setLine4Text(js.getString("Line4Text"));
                        schemelistmodel.setLine5Icon(js.getString("Line5Icon"));
                        schemelistmodel.setLine5Text(js.getString("Line5Text"));
                        schemelistmodel.setLine6Icon(js.getString("Line6Icon"));
                        schemelistmodel.setLine6Text(js.getString("Line6Text"));
                        listViewItems.add(schemelistmodel);

                    }
                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();

                } else if (getCode.equals("802")) {
                    Toast.makeText(getContext(), "Login Session Expired !", Toast.LENGTH_SHORT).show();
                    Task2 task = new Task2();
                    task.execute();

                } else {
                    Sneaker.with(getActivity())
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("This is the error message")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with(getActivity())
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("timeout",e.getMessage());
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading ");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class Task2 extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {


            String url = LOGOUT_URL + "?userid=" + getid + "&" + "token=" + gettoken;
            //  Log.e("logout", url);


            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);
                String getCode = jsonObject.getString("ErrorCode");
                if (getCode.equals("000")) {
                    Toast.makeText(getContext(), "Re Login!", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().getSupportFragmentManager().popBackStack();

                } else if (getCode.equals("802")) {

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                 //   Toast.makeText(getActivity(), "Login Again", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().getSupportFragmentManager().popBackStack();
                } else {
                    Sneaker.with(getActivity())
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("This is the error message")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with(getActivity())
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading ");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


}
