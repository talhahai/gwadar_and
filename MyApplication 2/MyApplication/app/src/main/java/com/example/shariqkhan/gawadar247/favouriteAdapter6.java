package com.example.shariqkhan.gawadar247;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.models.newsFlashModel;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShariqKhan on 10/26/2017.
 */

class favouriteAdapter6 extends RecyclerView.Adapter<favouriteAdapter6.Recyclerholder> {

    private ArrayList<newsFlashModel> arrayList = new ArrayList<>();
    public Context context;


    favouriteAdapter6(ArrayList<newsFlashModel> listViewItems, DistrictMap newsFeedActivity) {
        this.arrayList = listViewItems;
        this.context = newsFeedActivity;
    }

    @Override
    public Recyclerholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutfordistrict, parent, false);
        return new favouriteAdapter6.Recyclerholder(view);

    }

    @Override
    public void onBindViewHolder(final Recyclerholder holder, int position) {
        final newsFlashModel data = arrayList.get(position);
        holder.area.setText(data.date);
        Log.e("Name", data.date);
        Log.e("Thumb", data.thumb);

        Picasso.with(context).load(data.thumb).into(holder.circleImageView);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                final Dialog dialog = new Dialog(context);
//                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//
//                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                dialog.setContentView(R.layout.to_show_big_image);
//                ImageView img = (ImageView) dialog.findViewById(R.id.bigImage);
//                Picasso.with(context).load(data.image).into(img);
//                dialog.getWindow().setLayout(lp.width, lp.height);
//
//                dialog.show();

                Intent  intent = new Intent(context, ViewMapActivity.class);
                intent.putExtra("mapName", data.getDate());
                intent.putExtra("image", data.getImage());
                context.startActivity(intent);
            }

        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class Recyclerholder extends RecyclerView.ViewHolder {

        public TextView area;
        //  public static Button sendinquiry, addtocart;
        ImageView circleImageView;
        //   SimpleRatingBar ratingBar;
        View view;


        public Recyclerholder(View itemView) {
            super(itemView);
            view = itemView;


            area = (TextView) itemView.findViewById(R.id.districttext);

            circleImageView = (ImageView) itemView.findViewById(R.id.districtimage);


        }
    }

}
