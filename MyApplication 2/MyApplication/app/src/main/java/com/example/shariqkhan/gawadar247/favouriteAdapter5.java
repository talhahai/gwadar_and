package com.example.shariqkhan.gawadar247;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.models.newsFlashModel;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ShariqKhan on 10/26/2017.
 */

class favouriteAdapter5 extends RecyclerView.Adapter<favouriteAdapter5.Recyclerholder> {

    private ArrayList<newsFlashModel> arrayList = new ArrayList<>();
    public Context context;

    favouriteAdapter5(ArrayList<newsFlashModel> listViewItems, Context context) {
        this.arrayList = listViewItems;
        this.context = context;
    }

    @Override
    public Recyclerholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.newsflash_single_row, parent, false);
        return new favouriteAdapter5.Recyclerholder(view);

    }

    @Override
    public void onBindViewHolder(Recyclerholder holder, int position) {
        final newsFlashModel data = arrayList.get(position);
        holder.area.setText(data.getDate());
        Picasso.with(context).load(data.getImage()).into(holder.circleImageView);

        holder.circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ViewMapActivity.class);
                intent.putExtra("mapName", data.getDate());
                intent.putExtra("image", data.getImage());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class Recyclerholder extends RecyclerView.ViewHolder {

        public TextView area;
        //  public static Button sendinquiry, addtocart;
        ImageView circleImageView;
        //   SimpleRatingBar ratingBar;
        View view;
        RelativeLayout relativeLayout;

        public Recyclerholder(View itemView) {
            super(itemView);
            view = itemView;


            area = (TextView) itemView.findViewById(R.id.dateofnews);

            circleImageView = (ImageView) itemView.findViewById(R.id.imageOfNewsFlash);


        }
    }

}
