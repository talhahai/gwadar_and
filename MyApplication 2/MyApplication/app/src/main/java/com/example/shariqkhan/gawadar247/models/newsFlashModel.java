package com.example.shariqkhan.gawadar247.models;

/**
 * Created by ShariqKhan on 10/26/2017.
 */

public class newsFlashModel {
  public  String id;
   public String date;
   public String image;

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String thumb;

    public newsFlashModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
