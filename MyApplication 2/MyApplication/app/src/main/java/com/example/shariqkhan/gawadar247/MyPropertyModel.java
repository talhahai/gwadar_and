package com.example.shariqkhan.gawadar247;

/**
 * Created by ShariqKhan on 10/12/2017.
 */

public class MyPropertyModel {
    public String ID;
    public String UserID;
    public String Username;
    public String Phone;
    public String Rating;
    public String IsSold;
    public String Price;
    public String Area;
    public String PlotType;
    public String PlotNo;
    public String SchemeName;
    public String SubSchemeName;
    public String Layer1Name;
    public String Layer2Name;
    public String PlotID;
    public String IsFavourite;
    public String Image;
    public String IsRequested;

    public MyPropertyModel() {
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getIsSold() {
        return IsSold;
    }

    public void setIsSold(String isSold) {
        IsSold = isSold;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getPlotType() {
        return PlotType;
    }

    public void setPlotType(String plotType) {
        PlotType = plotType;
    }

    public String getPlotNo() {
        return PlotNo;
    }

    public void setPlotNo(String plotNo) {
        PlotNo = plotNo;
    }

    public String getSchemeName() {
        return SchemeName;
    }

    public void setSchemeName(String schemeName) {
        SchemeName = schemeName;
    }

    public String getSubSchemeName() {
        return SubSchemeName;
    }

    public void setSubSchemeName(String subSchemeName) {
        SubSchemeName = subSchemeName;
    }

    public String getLayer1Name() {
        return Layer1Name;
    }

    public void setLayer1Name(String layer1Name) {
        Layer1Name = layer1Name;
    }

    public String getLayer2Name() {
        return Layer2Name;
    }

    public void setLayer2Name(String layer2Name) {
        Layer2Name = layer2Name;
    }

    public String getPlotID() {
        return PlotID;
    }

    public void setPlotID(String plotID) {
        PlotID = plotID;
    }

    public String getIsFavourite() {
        return IsFavourite;
    }

    public void setIsFavourite(String isFavourite) {
        IsFavourite = isFavourite;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getIsRequested() {
        return IsRequested;
    }

    public void setIsRequested(String isRequested) {
        IsRequested = isRequested;
    }
}
