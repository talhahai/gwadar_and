package com.example.shariqkhan.gawadar247;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ShariqKhan on 10/24/2017.
 */

public class SearchReultActivityPager extends FragmentPagerAdapter {
    public String [] array;
    public SearchReultActivityPager(FragmentManager fm, String [] array) {
        super(fm);
        this.array = array;
    }
    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        args.putStringArray("Array", array);
        switch (position) {
            case 0:
                SearchSales wanted = new SearchSales();
                wanted.setArguments(args);
                return wanted;
            case 1:
                SearchWanted sales = new SearchWanted();
                sales.setArguments(args);
                return sales;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Sales";
            case 1:
                return "Wanted";
            default:
                return null;
        }

    }
}
