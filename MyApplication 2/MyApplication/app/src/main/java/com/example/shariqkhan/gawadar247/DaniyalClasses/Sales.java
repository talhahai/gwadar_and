package com.example.shariqkhan.gawadar247.DaniyalClasses;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.FavouriteSales;
import com.example.shariqkhan.gawadar247.LoginActivity;
import com.example.shariqkhan.gawadar247.NewsFeedActivity;
import com.example.shariqkhan.gawadar247.R;
import com.example.shariqkhan.gawadar247.adapters.AdapterInterface;
import com.example.shariqkhan.gawadar247.adapters.favouriteAdapter;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.BaseClass;
import com.example.shariqkhan.gawadar247.models.FavouriteModel;
import com.example.shariqkhan.gawadar247.models.NewsFeedModel;
import com.irozon.sneaker.Sneaker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by zameer on 15/08/2017.
 */
public class Sales extends Fragment implements AdapterInterface {
    @Nullable

    RecyclerView recyclerView;
    int checkForText = 0;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    public static String NEWSFEED_URL = "http://www.gwadar247.pk/api/newsfeed2.php?";
    SharedPreferences prefs;
    private String getuserid;
    public ProgressDialog progressDialog;
    private String gettoken;
    private Context context = getContext();
    ArrayList<NewsFeedModel> arrayList = new ArrayList<>();
    public String LOGOUT_URL = "http://www.gwadar247.pk/api/logout.php";
    private AdapterInterface adapterInterface;

    public void refreshFrag()
    {
        Sales sale = new Sales();
        getFragmentManager().beginTransaction().detach(sale).commit();
        getFragmentManager().beginTransaction().attach(sale).commit();
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sales, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler1);

        prefs = getContext().getSharedPreferences("SharedPreferences", MODE_PRIVATE);

        getuserid = prefs.getString("id", null);
        gettoken = prefs.getString("token", null);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);

        adapter = new favouriteAdapter(arrayList, getContext(),1, this);
        recyclerView.setAdapter(adapter);
        Task task = new Task();
        task.execute();

        return view;
    }

    @Override
    public void onRefresh() {
        adapter.notifyDataSetChanged();
    }

    private class Task extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {

            String url = NEWSFEED_URL + "userid=" + getuserid + "&" + "token=" + gettoken;

            Log.e("url", url);

            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                if (getCode.equals("000")) {
                    Log.e("Res", s);
                    // Toast.makeText(getContext(), "inside sales class", Toast.LENGTH_SHORT).show();

                    JSONObject propertiesObject
                            = jsonObject.getJSONObject("Properties");

                    JSONArray jsonArray = propertiesObject.getJSONArray("Sale");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        //  Log.e("loop", String.valueOf(i));

                        NewsFeedModel schemelistmodel = new NewsFeedModel();

                        JSONObject js = jsonArray.getJSONObject(i);


                        schemelistmodel.setID(js.getString("ID"));
                        schemelistmodel.setRating(js.getString("Rating"));
                        schemelistmodel.setIsSold(js.getString("IsSold"));
                        schemelistmodel.setIsOpenLand(js.getString("IsOpenLand"));
                        schemelistmodel.setPrice(js.getString("Price"));
                        schemelistmodel.setImage(js.getString("Image"));
                        schemelistmodel.setIsRequested(js.getString("IsRequested"));
                        schemelistmodel.setIsFavorite(js.getString("IsFavorite"));
                        schemelistmodel.setLine1Icon(js.getString("Line1Icon"));
                        schemelistmodel.setLine1Text(js.getString("Line1Text"));
                        schemelistmodel.setLine2Icon(js.getString("Line2Icon"));
                        schemelistmodel.setLine2Text(js.getString("Line2Text"));
                        schemelistmodel.setLine3Icon(js.getString("Line3Icon"));
                        schemelistmodel.setLine3Text(js.getString("Line3Text"));
                        schemelistmodel.setLine4Icon(js.getString("Line4Icon"));
                        schemelistmodel.setLine4Text(js.getString("Line4Text"));
                        schemelistmodel.setLine5Icon(js.getString("Line5Icon"));
                        schemelistmodel.setLine5Text(js.getString("Line5Text"));
                        schemelistmodel.setLine6Icon(js.getString("Line6Icon"));
                        schemelistmodel.setLine6Text(js.getString("Line6Text"));
                        //       Toast.makeText(getContext(), schemelistmodel.Layer2Name, Toast.LENGTH_SHORT).show();


                        arrayList.add(schemelistmodel);


                    }

                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();

                } else if (getCode.equals("802")) {
                    Toast.makeText(getContext(), "Login Session Expired !", Toast.LENGTH_SHORT).show();
                    Task2 task = new Task2();
                    task.execute();
                    progressDialog.dismiss();

                } else {
                    Sneaker.with(getActivity())
                            .setTitle("Error!!")
                            .setDuration(4000)
                            .setMessage("This is the error message")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with(getActivity())
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("timeout", e.getMessage());
                progressDialog.dismiss();
            }
            progressDialog.dismiss();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading ");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class Task2 extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {


            String url = LOGOUT_URL + "?userid=" + getuserid + "&" + "token=" + gettoken;
            //  Log.e("logout", url);


            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);
                String getCode = jsonObject.getString("ErrorCode");
                if (getCode.equals("000")) {
                    Toast.makeText(getContext(), "Re Login!", Toast.LENGTH_SHORT).show();

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().getSupportFragmentManager().popBackStack();

                } else if (getCode.equals("802")) {

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Toast.makeText(getActivity(), "Login Again", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().getSupportFragmentManager().popBackStack();
                } else {
                    Sneaker.with(getActivity())
                            .setTitle("Error!!")
                            .setDuration(4000)
                            .setMessage("This is the error message")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with(getActivity())
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading Plot of sales");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

}
