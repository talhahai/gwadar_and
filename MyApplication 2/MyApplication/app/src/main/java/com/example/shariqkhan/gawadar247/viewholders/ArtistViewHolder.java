package com.example.shariqkhan.gawadar247.viewholders;

/**
 * Created by ShariqKhan on 8/30/2017.
 */


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shariqkhan.gawadar247.R;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/**
 * Created by Brad on 12/18/2016.
 */

public class ArtistViewHolder extends ChildViewHolder {

    public TextView artistName;
    public TextView id;
    public ImageView artistImage;

    public View view;

    public ArtistViewHolder(View itemView) {
        super(itemView);
        view = itemView;
        artistImage = (ImageView) itemView.findViewById(R.id.list_item_artist_icon);

        artistName = (TextView) itemView.findViewById(R.id.list_item_artist_name);

    }

    public void setArtistName(String name) {
        artistName.setText(name);
    }

}