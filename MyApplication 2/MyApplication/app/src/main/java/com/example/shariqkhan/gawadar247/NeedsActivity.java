package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shariqkhan.gawadar247.DaniyalClasses.Data;
import com.example.shariqkhan.gawadar247.DaniyalClasses.Recycleradapter;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.FavouriteModel;
import com.example.shariqkhan.gawadar247.models.NewsFeedModel;
import com.irozon.sneaker.Sneaker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NeedsActivity extends AppCompatActivity {
    Toolbar toolbar;

    TextView toolbarText;

    public SharedPreferences prefs;
    private ProgressDialog progressDialog;
    String getid;
    String gettoken;
    public String LOGOUT_URL = "http://www.gwadar247.pk/api/logout.php";
    ArrayList<NewsFeedModel> listViewItems = new ArrayList<>();

    RecyclerView recyclerView;

    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    public static String FAVOURITE = "http://www.gwadar247.pk/api/myproperties2.php?";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_needs);

        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);
        toolbarText = (TextView) toolbar.findViewById(R.id.tollbarText);
        toolbarText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.property_recycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);


        prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        getid = prefs.getString("id", null);

        gettoken = prefs.getString("token", null);

        adapter = new favouriteAdapter4(listViewItems, this, true);

        recyclerView.setAdapter(adapter);

        Task task = new Task();
        task.execute();


    }

    private class Task extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {

            String url = FAVOURITE + "userid=" + getid + "&" + "token=" + gettoken;

            Log.e("url", url);

            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                String getCode = jsonObject.getString("ErrorCode");

                if (getCode.equals("000")) {

                    JSONObject propertiesObject
                            = jsonObject.getJSONObject("Properties");
                    JSONArray jsonArray = propertiesObject.getJSONArray("Wanted");
                    //                  Log.e("Properties",String.valueOf(jsonObject.getJSONArray("chuspa")));

                    for (int i = 0; i < jsonArray.length(); i++) {
                        // System.out.println("Inside for");
                        //Log.e("loop", String.valueOf(i));
                        NewsFeedModel schemelistmodel = new NewsFeedModel();
                        JSONObject js = jsonArray.getJSONObject(i);

                        schemelistmodel.setID(js.getString("ID"));
                        schemelistmodel.setRating(js.getString("Rating"));
                        schemelistmodel.setIsSold(js.getString("IsSold"));
                        schemelistmodel.setIsOpenLand(js.getString("IsOpenLand"));
                        schemelistmodel.setPrice(js.getString("Price"));
                        schemelistmodel.setImage(js.getString("Image"));
                        schemelistmodel.setIsRequested(js.getString("IsRequested"));
                        schemelistmodel.setIsFavorite(js.getString("IsFavorite"));
                        schemelistmodel.setLine1Icon(js.getString("Line1Icon"));
                        schemelistmodel.setLine1Text(js.getString("Line1Text"));
                        schemelistmodel.setLine2Icon(js.getString("Line2Icon"));
                        schemelistmodel.setLine2Text(js.getString("Line2Text"));
                        schemelistmodel.setLine3Icon(js.getString("Line3Icon"));
                        schemelistmodel.setLine3Text(js.getString("Line3Text"));
                        schemelistmodel.setLine4Icon(js.getString("Line4Icon"));
                        schemelistmodel.setLine4Text(js.getString("Line4Text"));
                        schemelistmodel.setLine5Icon(js.getString("Line5Icon"));
                        schemelistmodel.setLine5Text(js.getString("Line5Text"));
                        schemelistmodel.setLine6Icon(js.getString("Line6Icon"));
                        schemelistmodel.setLine6Text(js.getString("Line6Text"));
                       // schemelistmodel.IsFavourite = js.getString("IsFavorite");
                        listViewItems.add(schemelistmodel);

                        Log.e("Hellofromtheotherside", String.valueOf(listViewItems.get(i)));
                    }
                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();

                } else if (getCode.equals("802")) {


                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Intent intent = new Intent(NeedsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finishAffinity();


                } else {
                    Sneaker.with(NeedsActivity.this)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("This is the error message")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with(NeedsActivity.this)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("Neederror", e.getMessage());
                progressDialog.dismiss();
            }
            progressDialog.dismiss();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NeedsActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading Data");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    private class Task2 extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {


            String url = LOGOUT_URL + "?userid=" + getid + "&" + "token=" + gettoken;
            //  Log.e("logout", url);


            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);
                String getCode = jsonObject.getString("ErrorCode");
                if (getCode.equals("000")) {

                    Toast.makeText(NeedsActivity.this, "Re Login!", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Intent intent = new Intent(NeedsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                } else if (getCode.equals("802")) {

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Toast.makeText(NeedsActivity.this, "Login Again", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Intent intent = new Intent(NeedsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Sneaker.with(NeedsActivity.this)
                            .setTitle("Error!!")
                            .setDuration(4000)
                            .setMessage("This is the error message")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with(NeedsActivity.this)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                e.printStackTrace();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NeedsActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading Needed Plots");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

}
