package com.example.shariqkhan.gawadar247;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ShariqKhan on 10/23/2017.
 */

public class MyRequestPagerAdapter extends FragmentPagerAdapter {
    public MyRequestPagerAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                RequestSales wanted = new RequestSales();
                return wanted;
            case 1:
                RequestWanted sales = new RequestWanted();
                return sales;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Sales";
            case 1:
                return "Wanted";
            default:
                return null;
        }

    }
}
