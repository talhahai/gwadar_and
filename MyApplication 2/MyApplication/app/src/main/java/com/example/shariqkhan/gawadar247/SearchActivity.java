package com.example.shariqkhan.gawadar247;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.shariqkhan.gawadar247.adapters.GenreAdapter;
import com.example.shariqkhan.gawadar247.getdata.getHttpData;
import com.example.shariqkhan.gawadar247.models.Artist;
import com.example.shariqkhan.gawadar247.models.Genre;
import com.example.shariqkhan.gawadar247.models.NotificationModel;
import com.example.shariqkhan.gawadar247.models.SearchSchemeListModel;
import com.example.shariqkhan.gawadar247.models.SearchSubSchemeListModel;
import com.google.gson.JsonObject;
import com.irozon.sneaker.Sneaker;
import com.thoughtbot.expandablerecyclerview.listeners.OnGroupClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class SearchActivity extends AppCompatActivity implements SearchCallback {

   public static RecyclerView mRecyclerView;
    private GenreAdapter mAdapter;
    private List<Genre> genres;
    Toolbar toolbar;
    TextView toolbarText;
    TextView toolbarTextSearch;
    FloatingActionButton floatingActionButton;
    public static int check = 0;
    ImageView imageView;
    public static SearchActivity reference;
    public List<Artist> artists;
    ProgressDialog progressDialog;
    SharedPreferences prefs;
    String getid;
    String gettoken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        //reference = this;


        prefs = getSharedPreferences("SharedPreferences", MODE_PRIVATE);
        getid = prefs.getString("id", null);

        gettoken = prefs.getString("token", null);

        if (SearchReultActivity.searchReultActivity == null)
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);


//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(SearchActivity.this, MainActivity.class);
//                //  overridePendingTransition(0, R.anim.slide_out_right);
//
//                startActivity(intent);
//                finish();
//
//
//            }
//        });

        toolbar = (Toolbar) findViewById(R.id.main_app_bar);
        setSupportActionBar(toolbar);

        toolbarText = (TextView) toolbar.findViewById(R.id.tollbarText);
        toolbarText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //toolbarTextSearch = (TextView)toolbar.findViewById(R.id.toolbarText2);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingButton);
        floatingActionButton.setVisibility(View.INVISIBLE);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        Task task = new Task();
        task.execute();

    }


    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);

    }

    @Override
    public void onCompleted(ArrayList<String> sendlist) {
        Intent intent = new Intent(this, SearchReultActivity.class);
        intent.putStringArrayListExtra("Values", sendlist);
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_left);
        startActivity(intent);
    }

    private class Task extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... voids) {

            String url = "http://www.gwadar247.pk/api/searchsubchemelist.php?" + "userid=" + getid + "&" + "token=" + gettoken;

            Log.e("url", url);

            String response = getHttpData.getData(url);

            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            String getCode = "";
            try {
                if (s == null) {
                    throw new JSONException("No Internet Connection Exception");
                }

                super.onPostExecute(s);

                JSONObject jsonObject = new JSONObject(s);

                getCode = jsonObject.getString("ErrorCode");

                if (getCode.equals("000")) {

                    JSONArray jsonArray = jsonObject.getJSONArray("Schemes");

                    genres = new ArrayList<>(jsonArray.length());


                    for (int i = 0; i < jsonArray.length(); i++) {

                        Log.e("How much loop of sub", String.valueOf(jsonArray.length()));
                        JSONObject js = jsonArray.getJSONObject(i);
                        String ID = js.getString("ID");
                        String Name = js.getString("Name");
                        Log.e("Schemeid", ID);
                        Log.e("Schemename", Name);

                        JSONArray subArray = js.getJSONArray("Subschemes");

                        artists = new ArrayList<>(subArray.length());
                        for (int j = 0; j < subArray.length(); j++) {
                            Log.e("How much loop of sub", String.valueOf(subArray.length()));
                            JSONObject objectForSub = subArray.getJSONObject(j);
                            artists.add(new Artist(objectForSub.getString("ID"), objectForSub.getString("Name")));
                        }
                        genres.add(new Genre(Name , artists));
                    }
                    mAdapter = new GenreAdapter(genres, floatingActionButton, SearchActivity.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    mRecyclerView.setAdapter(mAdapter);
                    mRecyclerView.setLayoutManager(layoutManager);
                    mRecyclerView.setHasFixedSize(true);

                    progressDialog.dismiss();

                } else if (getCode.equals("802")) {

                    SharedPreferences.Editor edit = prefs.edit();
                    edit.clear();
                    edit.commit();
                    Intent intent = new Intent(SearchActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                } else {
                    Sneaker.with(SearchActivity.this)
                            .setTitle("Error!!")
                            .setDuration(5000)
                            .setMessage("This is the error message")
                            .sneakError();
                    progressDialog.dismiss();
                }

            } catch (JSONException e) {
                Sneaker.with(SearchActivity.this)
                        .setTitle("No internet connection!")
                        .setDuration(5000)
                        .setMessage("No internet connection. Retry with stable connection")
                        .sneakError();
                Log.e("SearchError", e.getMessage());
                Log.e("ErrorCode", getCode);
                progressDialog.dismiss();
            }
            progressDialog.dismiss();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SearchActivity.this, R.style.MyAlertDialogStyle);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please Wait");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

}
